
#include "TestPersistenceUtil.h"

#include "../test/field/TestFieldUtil.h"
#include "../test/structure/TestStructureUtil.h"
#include "../test/util/TestUtil.h"

#include "field/Character.h"
#include "field/Hex.h"
#include "field/IPv4.h"
#include "field/IntN.h"
#include "field/String.h"
#include "field/UTC.h"
#include "field/VarString.h"
#include "persistence/Loader.h"
#include "structure/BatchFormatter.h"

#include "gtest/gtest.h"

#include <fcntl.h>
#include <memory>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace persistence {
namespace {
template <typename CONCRETE>
void verify_type(const structure::StructFormatter::FieldP& pfield) {
    EXPECT_EQ(typeid(*pfield.get()), typeid(CONCRETE)) << pfield->name();
    EXPECT_EQ(typeid(*pfield.get()).name(), std::string(typeid(CONCRETE).name())) << pfield->name();
}
}

TEST(TestLoader, load_struct_all_types) {
    const auto name = util::TestUtil::create_text_file(TestUtil::ALL_TYPES_SPEC);
    const auto formatter = Loader::load_struct(name);
    EXPECT_EQ(formatter.static_size(), TestUtil::ALL_TYPES_STATIC_SIZE);
    size_t idx = 0;
    verify_type<field::String>(formatter.fields().at(idx++));
    verify_type<field::Character>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 1>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 2>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 3>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 4>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 5>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 6>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 7>>(formatter.fields().at(idx++));
    verify_type<field::IntN<true, 8>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 1>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 2>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 3>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 4>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 5>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 6>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 7>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 8>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 1>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 2>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 3>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 4>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 5>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 6>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 7>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<true, 8>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 1>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 2>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 3>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 4>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 5>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 6>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 7>>(formatter.fields().at(idx++));
    verify_type<field::BigEndianN<false, 8>>(formatter.fields().at(idx++));
    verify_type<field::IPv4>(formatter.fields().at(idx++));
    verify_type<field::Hex>(formatter.fields().at(idx++));
    verify_type<field::UTC32>(formatter.fields().at(idx++));
    verify_type<field::UTC64>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 2>>(formatter.fields().at(idx++));
    verify_type<field::IntN<false, 4>>(formatter.fields().at(idx++));
    verify_type<field::VarString>(formatter.fields().at(idx++));

    structure::TestUtil::verifyFormat(formatter,
          field::TestUtil::ALL_TYPES_BUFFER,
          sizeof(field::TestUtil::ALL_TYPES_BUFFER),
          field::TestUtil::ALL_TYPES_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::ALL_TYPES_NAMES_FOR_BUFFER,
          field::TestUtil::ALL_TYPES_VALUES_FOR_BUFFER);
}

TEST(TestLoader, load_struct_int) {
    const auto name = util::TestUtil::create_text_file(TestUtil::INT_SPEC);
    const auto formatter = Loader::load_struct(name);
    EXPECT_EQ(formatter.static_size(), field::TestUtil::INT_SIZE);

    structure::TestUtil::verifyFormat(formatter,
          field::TestUtil::BUFFER,
          sizeof(field::TestUtil::BUFFER),
          field::TestUtil::INT_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::INT_NAMES_FOR_BUFFER,
          field::TestUtil::INT_VALUES_FOR_BUFFER);
}

TEST(TestLoader, load_struct_bigendian) {
    const auto name = util::TestUtil::create_text_file(TestUtil::BE_SPEC);
    const auto formatter = Loader::load_struct(name);
    EXPECT_EQ(formatter.static_size(), field::TestUtil::BE_SIZE);

    structure::TestUtil::verifyFormat(formatter,
          field::TestUtil::BUFFER,
          sizeof(field::TestUtil::BUFFER),
          field::TestUtil::BE_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::BE_NAMES_FOR_BUFFER,
          field::TestUtil::BE_VALUES_FOR_BUFFER);
}

TEST(TestLoader, load_struct_varstring) {
    const auto spec_name = util::TestUtil::create_text_file(TestUtil::VARSTRING_SPEC);
    const auto struct_format = Loader::load_struct(spec_name);
    ASSERT_EQ(struct_format.fields().size(), 4u);
    verify_type<field::String>(struct_format.fields()[0]);
    verify_type<field::IntN<false, 1>>(struct_format.fields()[1]);
    verify_type<field::IntN<false, 1>>(struct_format.fields()[2]);
    verify_type<field::VarString>(struct_format.fields()[3]);
}

TEST(TestLoader, load_batch_int) {
    const auto spec_name = util::TestUtil::create_text_file(TestUtil::INT_SPEC);
    const auto batch_name = util::TestUtil::create_text_file("0 | name1 | " + spec_name + "\n10 | name2 | " + spec_name);
    const auto batch = Loader::load_batch(batch_name);
    ASSERT_EQ(batch.nodes().size(), 2u);
    EXPECT_EQ(batch.nodes()[0].name(), "name1");
    EXPECT_EQ(batch.nodes()[0].offset(), 0u);
    EXPECT_EQ(batch.nodes()[1].name(), "name2");
    EXPECT_EQ(batch.nodes()[1].offset(), 10u);
}

TEST(TestLoader, load_struct_utc) {
    const auto spec_name = util::TestUtil::create_text_file(TestUtil::UTC_SPEC);
    const auto struct_format = Loader::load_struct(spec_name);
    ASSERT_EQ(struct_format.fields().size(), 3u);
    verify_type<field::String>(struct_format.fields()[0]);
    verify_type<field::UTC32>(struct_format.fields()[1]);
    verify_type<field::UTC64>(struct_format.fields()[2]);
}

namespace {
void check_build_node(size_t offset,
      int size,
      size_t expected_number_of_fields,
      size_t size_of_last_field) {
    const auto node = Loader::build_node(offset, size);
    ASSERT_EQ(node.offset(), offset);
    ASSERT_EQ(node.formatter().fields().size(), expected_number_of_fields);

    for (size_t i = 0; i < expected_number_of_fields; ++i) {
        verify_type<field::String>(node.formatter().fields()[i]);
        ASSERT_EQ(node.formatter().fields()[i]->static_offset(), 8 * i);
        if (i + 1 == expected_number_of_fields)
            ASSERT_EQ(node.formatter().fields()[i]->static_size(), size_of_last_field);
        else
            ASSERT_EQ(node.formatter().fields()[i]->static_size(), 8u);
    }
}
}

TEST(TestLoader, build_node) {
    auto node = Loader::build_node(10, 17);
    ASSERT_EQ(node.offset(), 10u);
    ASSERT_EQ(node.formatter().fields().size(), 3u);
    verify_type<field::String>(node.formatter().fields()[0]);
    verify_type<field::String>(node.formatter().fields()[1]);
    verify_type<field::String>(node.formatter().fields()[2]);

    ASSERT_EQ(node.formatter().fields()[0]->static_offset(), 0u);
    ASSERT_EQ(node.formatter().fields()[1]->static_offset(), 8u);
    ASSERT_EQ(node.formatter().fields()[2]->static_offset(), 16u);

    ASSERT_EQ(node.formatter().fields()[0]->static_size(), 8u);
    ASSERT_EQ(node.formatter().fields()[1]->static_size(), 8u);
    ASSERT_EQ(node.formatter().fields()[2]->static_size(), 1u);

    check_build_node(37, 2, 1, 2);
    check_build_node(29, 8, 1, 8);
    check_build_node(29, 9, 2, 1);
    check_build_node(97, 16, 2, 8);
    check_build_node(10, 20, 3, 4);
}
}
