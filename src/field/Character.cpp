#include "Character.h"

#include <cctype>

namespace field {
Character::Character(std::string name, const size_t offset)
      : StaticDimensions(std::move(name), offset, 1) {
}

std::string Character::do_format(const uint8_t* buffer) const {
    const auto off = dynamic_offset(buffer);
    if (std::isprint(buffer[off])) {
        return std::string(1, buffer[off]);
    } else {
        return ".";
    }
}
}
