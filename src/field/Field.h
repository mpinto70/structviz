#pragma once

#include <cstdint>
#include <string>

namespace field {
class Field {
public:
    explicit Field(std::string name);
    virtual ~Field() noexcept = default;
    virtual size_t static_offset() const = 0;
    virtual size_t dynamic_offset(const uint8_t* buffer) const = 0;
    virtual size_t static_size() const = 0;
    virtual size_t dynamic_size(const uint8_t* buffer) const = 0;
    bool is_valid(const uint8_t* buffer, size_t buffer_size) const;
    std::string format(const uint8_t* buffer) const;
    const std::string& name() const { return m_name; }

private:
    const std::string m_name;

    virtual bool can_compute_offset_size(const uint8_t* buffer, size_t buffer_size) const = 0;
    virtual std::string do_format(const uint8_t* buffer) const = 0;
};
}
