#pragma once

#include "UI.h"

#include <curses.h>

namespace ui {

class Curses : public UI {
public:
    Curses();
    ~Curses() noexcept override;
    Operation get_input() const override;
    void show(const structure::BatchFormatter& batch, size_t index, std::istream& data) override;
    NodeParameters get_node_parameters(const structure::BatchFormatter& batch, size_t index) const override;
    NodeParameters get_fixed_parameters(const structure::BatchFormatter& batch, size_t index) const override;
    int get_buffer_size() const override;
    std::string get_batch_name(const std::string& current_name) const override;
    void show_exception_and_wait_confirm(const std::string& msg, const std::exception& e) const override;
    void wait_confirm(const std::string& msg) const override;
    bool want_to_save_first() const override;

private:
    WINDOW* m_win;
};
}
