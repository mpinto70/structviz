#pragma once

#include "field/StaticDimensions.h"

#include <type_traits>

namespace field {

template <bool is_signed>
class Numeric : public StaticDimensions {
public:
    using ValueT = std::conditional_t<is_signed, int64_t, uint64_t>;

    Numeric(std::string name,
          size_t offset,
          size_t size)
          : StaticDimensions(std::move(name), offset, size) {
    }
    ~Numeric() noexcept override = default;
    virtual ValueT value(const uint8_t* buffer) const = 0;
};
}
