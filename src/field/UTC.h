#pragma once

#include "field/StaticDimensions.h"

namespace field {

class UTC32 : public StaticDimensions {
public:
    UTC32(std::string name, size_t offset);
    ~UTC32() noexcept override = default;

private:
    std::string do_format(const uint8_t* buffer) const override;
};

class UTC64 : public StaticDimensions {
public:
    UTC64(std::string name, size_t offset);
    ~UTC64() noexcept override = default;

private:
    std::string do_format(const uint8_t* buffer) const override;
};
}
