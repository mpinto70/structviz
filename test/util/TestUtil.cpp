
#include "TestUtil.h"

#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <fstream>
#include <gtest/gtest.h>
#include <unistd.h>

namespace util {

namespace {
template <typename T>
void append_file(const std::string& name, std::ostream& out, const T& t) {
    out.write((const char*) t.data(), t.size());
    ASSERT_TRUE(out) << "writing to " << name;
}

template <typename T>
std::string create_file(const std::string& name, const T& t) {
    std::ofstream out(name);
    EXPECT_TRUE(out) << "opening " << name;
    append_file<T>(name, out, t);
    return name;
}
}

std::vector<std::string> TestUtil::m_files_created;

std::string TestUtil::create_text_file() {
    return create_tmp_file("/tmp/test.file.txt");
}

std::string TestUtil::create_binary_file() {
    return create_tmp_file("/tmp/test.file.bin");
}

std::string TestUtil::create_text_file(const std::string& content) {
    return create_file(create_text_file(), content);
}

std::string TestUtil::create_binary_file(const std::vector<uint8_t>& content) {
    return create_file(create_binary_file(), content);
}

void TestUtil::append_text(std::ostream& out, const std::string& content) {
    append_file("file", out, content);
}

void TestUtil::append_binary(std::ostream& out, const std::vector<uint8_t>& content) {
    append_file("file", out, content);
}

void TestUtil::append_binary(std::ostream& out, const std::string& content) {
    append_binary(out, std::vector<uint8_t>(content.begin(), content.end()));
}

std::string TestUtil::create_tmp_file(const std::string& base_name) {
    char filename[256];
    if (base_name.length() > 248) {
        throw std::invalid_argument("too long name " + base_name);
    }
    strcpy(filename, base_name.c_str());
    strcat(filename, ".XXXXXX");

    int fp = mkstemp(filename);
    EXPECT_NE(fp, -1) << "creating temporary for " << base_name << " - " << strerror(errno);
    close(fp);

    m_files_created.push_back(filename);
    return filename;
}

class EnvironmentCleanup : public ::testing::Environment {
    void TearDown() override;
};

// registering cleanup
::testing::Environment* const environment_cleanup = ::testing::AddGlobalTestEnvironment(new EnvironmentCleanup);

void EnvironmentCleanup::TearDown() {
    assert(environment_cleanup != nullptr);
    // delete all created files
    for (const auto& file : TestUtil::files_created()) {
        unlink(file.c_str());
    }
    TestUtil::clear_files();
}
}
