
#include "../test/field/TestFieldUtil.h"
#include "../test/structure/TestStructureUtil.h"
#include "../test/util/TestUtil.h"

#include "structure/BatchFormatter.h"

#include <gtest/gtest.h>

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <sstream>

namespace structure {

TEST(TestBatchFormatter, Struct) {
    const Struct node(12, "name", StructFormatter("", structure::TestUtil::UINT_field_list_for_BUFFER()));

    EXPECT_EQ(node.offset(), 12u);
    EXPECT_EQ(node.name(), "name");
    EXPECT_EQ(node.formatter().static_size(), field::TestUtil::INT_SIZE);
}

TEST(TestBatchFormatter, create) {
    BatchFormatter::NodeListT nodes;
    nodes.emplace_back(17, "some name", StructFormatter("", structure::TestUtil::UINT_field_list_for_BUFFER()));
    nodes.emplace_back(1543, "other name", StructFormatter("", structure::TestUtil::BE_field_list_for_BUFFER()));
    const BatchFormatter batch(std::move(nodes));
    EXPECT_EQ(batch.nodes().size(), 2u);

    EXPECT_EQ(batch.nodes()[0].offset(), 17u);
    EXPECT_EQ(batch.nodes()[0].name(), "some name");
    EXPECT_EQ(batch.nodes()[0].formatter().static_size(), field::TestUtil::INT_SIZE);
    EXPECT_EQ(batch.nodes()[1].offset(), 1543u);
    EXPECT_EQ(batch.nodes()[1].name(), "other name");
    EXPECT_EQ(batch.nodes()[1].formatter().static_size(), field::TestUtil::BE_SIZE);
}

TEST(TestBatchFormatter, add) {
    BatchFormatter batch(BatchFormatter::NodeListT{});
    EXPECT_EQ(batch.nodes().size(), 0u);

    batch.nodes().emplace_back(15, "some name", StructFormatter("", structure::TestUtil::UINT_field_list_for_BUFFER()));
    ASSERT_EQ(batch.nodes().size(), 1u);

    EXPECT_EQ(batch.nodes()[0].offset(), 15u);
    EXPECT_EQ(batch.nodes()[0].name(), "some name");
    EXPECT_EQ(batch.nodes()[0].formatter().static_size(), field::TestUtil::INT_SIZE);

    batch.nodes().emplace_back(150, "other name", StructFormatter("", structure::TestUtil::BE_field_list_for_BUFFER()));
    ASSERT_EQ(batch.nodes().size(), 2u);

    EXPECT_EQ(batch.nodes()[0].offset(), 15u);
    EXPECT_EQ(batch.nodes()[0].name(), "some name");
    EXPECT_EQ(batch.nodes()[0].formatter().static_size(), field::TestUtil::INT_SIZE);

    EXPECT_EQ(batch.nodes()[1].offset(), 150u);
    EXPECT_EQ(batch.nodes()[1].name(), "other name");
    EXPECT_EQ(batch.nodes()[1].formatter().static_size(), field::TestUtil::BE_SIZE);
}

TEST(TestBatchFormatter, remove) {
    BatchFormatter::NodeListT nodes;
    nodes.emplace_back(17, "some name", StructFormatter("", structure::TestUtil::UINT_field_list_for_BUFFER()));
    nodes.emplace_back(1543, "other name", StructFormatter("", structure::TestUtil::BE_field_list_for_BUFFER()));
    BatchFormatter batch(std::move(nodes));
    EXPECT_EQ(batch.nodes().size(), 2u);
    EXPECT_EQ(batch.nodes()[0].offset(), 17u);
    EXPECT_EQ(batch.nodes()[0].name(), "some name");
    EXPECT_EQ(batch.nodes()[0].formatter().static_size(), field::TestUtil::INT_SIZE);
    EXPECT_EQ(batch.nodes()[1].offset(), 1543u);
    EXPECT_EQ(batch.nodes()[1].name(), "other name");
    EXPECT_EQ(batch.nodes()[1].formatter().static_size(), field::TestUtil::BE_SIZE);

    EXPECT_THROW(batch.remove(2), std::out_of_range);

    EXPECT_EQ(batch.nodes().size(), 2u);
    EXPECT_EQ(batch.nodes()[0].offset(), 17u);
    EXPECT_EQ(batch.nodes()[0].name(), "some name");
    EXPECT_EQ(batch.nodes()[0].formatter().static_size(), field::TestUtil::INT_SIZE);
    EXPECT_EQ(batch.nodes()[1].offset(), 1543u);
    EXPECT_EQ(batch.nodes()[1].name(), "other name");
    EXPECT_EQ(batch.nodes()[1].formatter().static_size(), field::TestUtil::BE_SIZE);

    EXPECT_NO_THROW(batch.remove(0));

    EXPECT_EQ(batch.nodes().size(), 1u);
    EXPECT_EQ(batch.nodes()[0].offset(), 1543u);
    EXPECT_EQ(batch.nodes()[0].name(), "other name");
    EXPECT_EQ(batch.nodes()[0].formatter().static_size(), field::TestUtil::BE_SIZE);
}

TEST(TestBatchFormatter, format) {
    BatchFormatter batch(BatchFormatter::NodeListT{});
    batch.nodes().emplace_back(1 + 0 * sizeof(field::TestUtil::BUFFER), "first", StructFormatter("", structure::TestUtil::UINT_field_list_for_BUFFER()));
    batch.nodes().emplace_back(2 + 1 * sizeof(field::TestUtil::BUFFER), "second", StructFormatter("", structure::TestUtil::BE_field_list_for_BUFFER()));
    batch.nodes().emplace_back(2 + 2 * sizeof(field::TestUtil::BUFFER), "third", StructFormatter("", structure::TestUtil::BE_field_list_for_BUFFER()));

    const auto bin_content = structure::TestUtil::create_content_for_batch();
    const auto content = std::string(bin_content.begin(), bin_content.end());
    std::istringstream in(content);
    ASSERT_TRUE(in);

    structure::TestUtil::verifyFormatted(batch.format(0, in).fields,
          field::TestUtil::INT_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::INT_NAMES_FOR_BUFFER,
          field::TestUtil::UINT_VALUES_FOR_BUFFER);
    structure::TestUtil::verifyFormatted(batch.format(1, in).fields,
          field::TestUtil::BE_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::BE_NAMES_FOR_BUFFER,
          field::TestUtil::BE_VALUES_FOR_BUFFER);
    structure::TestUtil::verifyFormatted(batch.format(2, in).fields,
          field::TestUtil::BE_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::BE_NAMES_FOR_BUFFER,
          field::TestUtil::BE_VALUES_FOR_BUFFER);
}
}
