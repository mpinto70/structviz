#pragma once

#include "structure/StructFormatter.h"

#include <cstdlib>
#include <istream>

namespace structure {

class Struct {
public:
    Struct(size_t offset,
          const std::string& name,
          StructFormatter&& formatter);
    size_t offset() const { return m_offset; }
    const std::string& name() const { return m_name; }
    const std::string& origin() const { return m_formatter.origin(); }
    const StructFormatter& formatter() const { return m_formatter; }

private:
    size_t m_offset;
    std::string m_name;
    StructFormatter m_formatter;
};

struct FormattedStruct {
    const StructFormatter::FormattedFieldListT fields;
    const std::vector<uint8_t> buffer;
};

class BatchFormatter {
public:
    using NodeListT = std::vector<Struct>;
    explicit BatchFormatter(NodeListT&& nodes);
    const NodeListT& nodes() const { return m_nodes; }
    NodeListT& nodes() { return m_nodes; }
    void remove(size_t index);
    FormattedStruct format(size_t index, std::istream& in) const;

private:
    NodeListT m_nodes;
};
}
