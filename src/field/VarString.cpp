#include "VarString.h"
#include "field/Util.h"

#include <algorithm>

namespace field {
VarString::VarString(std::string name,
      const Numeric<false>* offset_field,
      const Numeric<false>* size_field)
      : DynamicDimensions(std::move(name), offset_field, size_field),
        m_character("for VarString", 0) {
}

std::string VarString::do_format(const uint8_t* buffer) const {
    const size_t off = dynamic_offset(buffer);
    const size_t siz = dynamic_size(buffer);
    auto concatenated = std::for_each(&buffer[off], &buffer[off + siz], Concatenator<Character>(m_character));
    return concatenated.result;
}
}
