#include "UTC.h"

#include <iomanip>
#include <sstream>

namespace field {

namespace {
std::string format_date(const time_t dt) {
    tm the_tm{};
    gmtime_r(&dt, &the_tm);
    char buffer[32];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &the_tm);
    return buffer;
}
}

UTC32::UTC32(std::string name, size_t offset)
      : StaticDimensions(std::move(name), offset, 4) {
}

std::string UTC32::do_format(const uint8_t* buffer) const {
    const auto off = dynamic_offset(buffer);
    auto dt = reinterpret_cast<const int32_t*>(buffer + off);
    return format_date(*dt);
}

UTC64::UTC64(std::string name, size_t offset)
      : StaticDimensions(std::move(name), offset, 8) {
}

std::string UTC64::do_format(const uint8_t* buffer) const {
    constexpr uint64_t NSECS_PER_SEC = 1000000000;
    const auto off = dynamic_offset(buffer);
    auto dt = reinterpret_cast<const int64_t*>(buffer + off);
    std::ostringstream out;
    out << format_date(*dt / NSECS_PER_SEC) << '.';
    out << std::setw(9) << std::setfill('0') << (*dt % NSECS_PER_SEC);
    return out.str();
}
}
