
#include "TestFieldUtil.h"
#include "../test/util/TestUtil.h"

#include "field/Character.h"

#include "gtest/gtest.h"

namespace field {

constexpr uint8_t TestUtil::ONE_BYTE[];
constexpr uint8_t TestUtil::BUFFER[];

const std::vector<std::pair<size_t, size_t>> TestUtil::INT_OFFSETS_SIZES_FOR_BUFFER = {
    { 0, 13 },
    { 13, 8 },
    { 21, 1 },
    { 22, 1 },
    { 23, 2 },
    { 25, 4 },
    { 29, 8 },
};
const std::vector<std::pair<size_t, size_t>> TestUtil::BE_OFFSETS_SIZES_FOR_BUFFER = {
    { 0, 13 },
    { 13, 8 },
    { 21, 1 },
    { 22, 1 },
    { 23, 2 },
    { 25, 4 },
    { 29, 8 },
    { 37, 7 },
};
const std::vector<std::string> TestUtil::INT_NAMES_FOR_BUFFER = {
    "name",
    "eight",
    "char",
    "value8",
    "value16",
    "value32",
    "value64",
};
const std::vector<std::string> TestUtil::INT_VALUES_FOR_BUFFER = {
    "MARCELO PINTO",
    "..... ..",
    "H",
    "72",
    "258",
    "1144201745",
    "-1167088121787636991",
};
const std::vector<std::string> TestUtil::UINT_VALUES_FOR_BUFFER = {
    "MARCELO PINTO",
    "..... ..",
    "H",
    "72",
    "258",
    "1144201745",
    "17279655951921914625",
};
const std::vector<std::string> TestUtil::BE_NAMES_FOR_BUFFER = {
    "name",
    "eight",
    "char",
    "value8",
    "value16",
    "value32",
    "value64",
    "garbage"
};
const std::vector<std::string> TestUtil::BE_VALUES_FOR_BUFFER = {
    "MARCELO PINTO",
    "..... ..",
    "H",
    "72",
    "513",
    "287454020",
    "81985529216486895",
    "GARBAGE",
};
const std::vector<std::string> TestUtil::UBE_VALUES_FOR_BUFFER = {
    "MARCELO PINTO",
    "..... ..",
    "H",
    "72",
    "513",
    "287454020",
    "81985529216486895",
    "GARBAGE",
};
constexpr size_t TestUtil::INT_SIZE;
constexpr size_t TestUtil::BE_SIZE;

constexpr uint8_t TestUtil::ALL_TYPES_BUFFER[];
const std::vector<std::pair<size_t, size_t>> TestUtil::ALL_TYPES_OFFSETS_SIZES_FOR_BUFFER = {
    { 0, 13 },
    { 13, 1 },
    // int and uint
    { 14, 1 },
    { 15, 2 },
    { 17, 3 },
    { 20, 4 },
    { 24, 5 },
    { 29, 6 },
    { 35, 7 },
    { 42, 8 },
    { 50, 1 },
    { 51, 2 },
    { 53, 3 },
    { 56, 4 },
    { 60, 5 },
    { 65, 6 },
    { 71, 7 },
    { 78, 8 },
    // be and ube
    { 86, 1 },
    { 87, 2 },
    { 89, 3 },
    { 92, 4 },
    { 96, 5 },
    { 101, 6 },
    { 107, 7 },
    { 114, 8 },
    { 122, 1 },
    { 123, 2 },
    { 125, 3 },
    { 128, 4 },
    { 132, 5 },
    { 137, 6 },
    { 143, 7 },
    { 150, 8 },
    // IP
    { 158, 4 },
    // hex (14)
    { 162, 14 },
    // utc
    { 176, 4 },
    { 180, 8 },
    // varstring
    { 188, 2 },
    { 190, 4 },
    { 194, 7 },
};

const std::vector<std::string> TestUtil::ALL_TYPES_NAMES_FOR_BUFFER = {
    "value for fixed string",
    "value for single char",
    "value for int 8",
    "value for int 16",
    "value for int 24",
    "value for int 32",
    "value for int 40",
    "value for int 48",
    "value for int 56",
    "value for int 64",
    "value for unsigned int 8",
    "value for unsigned int 16",
    "value for unsigned int 24",
    "value for unsigned int 32",
    "value for unsigned int 40",
    "value for unsigned int 48",
    "value for unsigned int 56",
    "value for unsigned int 64",
    "value for big endian 8",
    "value for big endian 16",
    "value for big endian 24",
    "value for big endian 32",
    "value for big endian 40",
    "value for big endian 48",
    "value for big endian 56",
    "value for big endian 64",
    "value for unsigned big endian 8",
    "value for unsigned big endian 16",
    "value for unsigned big endian 24",
    "value for unsigned big endian 32",
    "value for unsigned big endian 40",
    "value for unsigned big endian 48",
    "value for unsigned big endian 56",
    "value for unsigned big endian 64",
    "value IPV4",
    "fixed hexadecimal",
    "utc time in seconds",
    "utc time in nano seconds",
    "size_field",
    "offset_field",
    "a variable sized string",
};

const std::vector<std::string> TestUtil::ALL_TYPES_VALUES_FOR_BUFFER = {
    "MARCELO PINTO",
    "C",
    "-94",
    "-24009",
    "-6146080",
    "-1573396430",
    "-402789485892",
    "-103114108388190",
    "-26397211747376488",
    "-6757686207328380927",
    "162",
    "41527",
    "10631136",
    "2721570866",
    "696722141884",
    "178360868322466",
    "45660382290551448",
    "11689057866381170689",
    "-94",
    "-24009",
    "-6146080",
    "-1573396430",
    "-402789485892",
    "-103114108388190",
    "-26397211747376488",
    "-6757686207328380927",
    "162",
    "41527",
    "10631136",
    "2721570866",
    "696722141884",
    "178360868322466",
    "45660382290551448",
    "11689057866381170689",
    "192.168.2.1",
    "a2 37 e0 32 bc a2 98 01 a2 37 e0 32 bc a2",
    "1998-07-30 20:15:34",
    "1998-07-30 20:15:34.987654321",
    "7",
    "194",
    "Almeida",
};

void TestUtil::verifyFormat(const Field& field,
      const uint8_t* buffer,
      const std::string& output) {
    EXPECT_EQ(field.format(buffer), output) << field.name();
}
}
