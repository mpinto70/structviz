#include "persistence/Loader.h"
#include "field/Character.h"
#include "field/Hex.h"
#include "field/IPv4.h"
#include "field/IntN.h"
#include "field/String.h"
#include "field/UTC.h"
#include "field/VarString.h"
#include "util/Util.h"

#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdexcept>

namespace persistence {

namespace {

constexpr size_t INT8_SIZE = 8;
constexpr size_t INT16_SIZE = 16;
constexpr size_t INT24_SIZE = 24;
constexpr size_t INT32_SIZE = 32;
constexpr size_t INT40_SIZE = 40;
constexpr size_t INT48_SIZE = 48;
constexpr size_t INT56_SIZE = 56;
constexpr size_t INT64_SIZE = 64;

void split(std::string str, const char delim, std::vector<std::string>& elems) {
    while (not util::trim(str).empty()) {
        std::stringstream ss;
        ss.str(str);
        std::string item;
        std::getline(ss, item, delim);
        str.erase(0, ss.tellg());
        util::trim(item);
        elems.push_back(item);
    }
}

std::vector<std::string> split(const std::string& str, char delim) {
    std::vector<std::string> elems;
    split(str, delim, elems);
    return elems;
}

enum class FieldType {
    Char,
    Int,
    UInt,
    Be,
    UBe,
    IP,
    Hex,
    VarString,
    UTC,
};

struct FieldToken {
    FieldToken(FieldType type, size_t size, std::string name)
          : type(type),
            size(size),
            name(std::move(name)),
            field_offset(""),
            field_size("") {
    }
    FieldToken(FieldType type,
          std::string name,
          std::string field_offset,
          std::string field_size)
          : type(type),
            size(0),
            name(std::move(name)),
            field_offset(std::move(field_offset)),
            field_size(std::move(field_size)) {
    }

    const FieldType type;
    const size_t size;
    const std::string name;
    const std::string field_offset;
    const std::string field_size;
};

FieldToken tokenize_field(const std::string& line) {
    const auto tokens = split(line, '|');
    if (tokens.size() != 3) {
        throw std::invalid_argument("Could not tokenize field [" + line + "] properly: " + std::to_string(tokens.size()));
    }

    if (tokens[0] == "varstring") {
        const auto fields = split(tokens[1], ',');
        if (fields.size() != 2) {
            throw std::invalid_argument("Invalid number of fields in varstring [" + line + "]");
        }
        return FieldToken(FieldType::VarString, tokens[2], fields[0], fields[1]);
    } else {
        std::string::size_type sz;
        const size_t size = std::stoul(tokens[1], &sz);
        if (sz != tokens[1].size() || size == 0) {
            throw std::invalid_argument("Could not tokenize field [" + line + "] properly: invalid size " + tokens[1]);
        }
        if (tokens[0] == "char") {
            return FieldToken(FieldType::Char, size, tokens[2]);
        } else if (tokens[0] == "int" || tokens[0] == "be" || tokens[0] == "uint" || tokens[0] == "ube") {
            switch (size) {
                case INT8_SIZE:
                case INT16_SIZE:
                case INT24_SIZE:
                case INT32_SIZE:
                case INT40_SIZE:
                case INT48_SIZE:
                case INT56_SIZE:
                case INT64_SIZE:
                    break;
                default:
                    throw std::invalid_argument("Could not tokenize field [" + line + "] properly: invalid size for number " + tokens[1]);
            }
            if (tokens[0] == "int") {
                return FieldToken(FieldType::Int, size, tokens[2]);
            } else if (tokens[0] == "uint") {
                return FieldToken(FieldType::UInt, size, tokens[2]);
            } else if (tokens[0] == "be") {
                return FieldToken(FieldType::Be, size, tokens[2]);
            } else {
                return FieldToken(FieldType::UBe, size, tokens[2]);
            }
        } else if (tokens[0] == "IP") {
            return FieldToken(FieldType::IP, size, tokens[2]);
        } else if (tokens[0] == "hex") {
            return FieldToken(FieldType::Hex, size, tokens[2]);
        } else if (tokens[0] == "utc") {
            return FieldToken(FieldType::UTC, size, tokens[2]);
        } else {
            throw std::invalid_argument("Could not tokenize field [" + line + "] properly: invalid type " + tokens[0]);
        }
    }
}

field::Field* create_Int(const FieldToken& token, size_t offset) {
    switch (token.size) {
        case INT8_SIZE:
            return new field::IntN<true, 1>(token.name, offset);
        case INT16_SIZE:
            return new field::IntN<true, 2>(token.name, offset);
        case INT24_SIZE:
            return new field::IntN<true, 3>(token.name, offset);
        case INT32_SIZE:
            return new field::IntN<true, 4>(token.name, offset);
        case INT40_SIZE:
            return new field::IntN<true, 5>(token.name, offset);
        case INT48_SIZE:
            return new field::IntN<true, 6>(token.name, offset);
        case INT56_SIZE:
            return new field::IntN<true, 7>(token.name, offset);
        case INT64_SIZE:
            return new field::IntN<true, 8>(token.name, offset);
        default:
            throw std::invalid_argument("Could not create numeric value: invalid size " + std::to_string(token.size));
    }
}

field::Field* create_UInt(const FieldToken& token, size_t offset) {
    switch (token.size) {
        case INT8_SIZE:
            return new field::IntN<false, 1>(token.name, offset);
        case INT16_SIZE:
            return new field::IntN<false, 2>(token.name, offset);
        case INT24_SIZE:
            return new field::IntN<false, 3>(token.name, offset);
        case INT32_SIZE:
            return new field::IntN<false, 4>(token.name, offset);
        case INT40_SIZE:
            return new field::IntN<false, 5>(token.name, offset);
        case INT48_SIZE:
            return new field::IntN<false, 6>(token.name, offset);
        case INT56_SIZE:
            return new field::IntN<false, 7>(token.name, offset);
        case INT64_SIZE:
            return new field::IntN<false, 8>(token.name, offset);
        default:
            throw std::invalid_argument("Could not create numeric value: invalid size " + std::to_string(token.size));
    }
}

field::Field* create_Be(const FieldToken& token, size_t offset) {
    switch (token.size) {
        case INT8_SIZE:
            return new field::BigEndianN<true, 1>(token.name, offset);
        case INT16_SIZE:
            return new field::BigEndianN<true, 2>(token.name, offset);
        case INT24_SIZE:
            return new field::BigEndianN<true, 3>(token.name, offset);
        case INT32_SIZE:
            return new field::BigEndianN<true, 4>(token.name, offset);
        case INT40_SIZE:
            return new field::BigEndianN<true, 5>(token.name, offset);
        case INT48_SIZE:
            return new field::BigEndianN<true, 6>(token.name, offset);
        case INT56_SIZE:
            return new field::BigEndianN<true, 7>(token.name, offset);
        case INT64_SIZE:
            return new field::BigEndianN<true, 8>(token.name, offset);
        default:
            throw std::invalid_argument("Could not create numeric value: invalid size " + std::to_string(token.size));
    }
}

field::Field* create_UBe(const FieldToken& token, size_t offset) {
    switch (token.size) {
        case INT8_SIZE:
            return new field::BigEndianN<false, 1>(token.name, offset);
        case INT16_SIZE:
            return new field::BigEndianN<false, 2>(token.name, offset);
        case INT24_SIZE:
            return new field::BigEndianN<false, 3>(token.name, offset);
        case INT32_SIZE:
            return new field::BigEndianN<false, 4>(token.name, offset);
        case INT40_SIZE:
            return new field::BigEndianN<false, 5>(token.name, offset);
        case INT48_SIZE:
            return new field::BigEndianN<false, 6>(token.name, offset);
        case INT56_SIZE:
            return new field::BigEndianN<false, 7>(token.name, offset);
        case INT64_SIZE:
            return new field::BigEndianN<false, 8>(token.name, offset);
        default:
            throw std::invalid_argument("Could not create numeric value: invalid size " + std::to_string(token.size));
    }
}

field::Field* create_IP(const FieldToken& token, size_t offset) {
    switch (token.size) {
        case 4:
            return new field::IPv4(token.name, offset);
        default:
            throw std::invalid_argument("Could not create IP: invalid size " + std::to_string(token.size) + ". Only IPv4 is supported");
    }
}

field::Field* create_UTC(const FieldToken& token, size_t offset) {
    switch (token.size) {
        case 32:
            return new field::UTC32(token.name, offset);
        case 64:
            return new field::UTC64(token.name, offset);
        default:
            throw std::invalid_argument("Could not create UTC: invalid size " + std::to_string(token.size));
    }
}

struct NodeToken {
    NodeToken(size_t offset,
          std::string name,
          std::string path)
          : offset(offset),
            name(std::move(name)),
            path(std::move(path)) {
    }

    const size_t offset;
    const std::string name;
    const std::string path;
};

NodeToken tokenize_node(const std::string& line) {
    const auto tokens = split(line, '|');
    if (tokens.size() != 3) {
        throw std::invalid_argument("Could not tokenize node [" + line + "] properly: " + std::to_string(tokens.size()));
    }

    std::string::size_type sz;
    const size_t offset = std::stoul(tokens[0], &sz);
    if (sz != tokens[0].size()) {
        throw std::invalid_argument("Could not tokenize node [" + line + "] properly: invalid offset " + tokens[0]);
    }

    return NodeToken(offset, tokens[1], tokens[2]);
}

const field::Numeric<false>* get_numeric(const field::Field* fld) {
    auto pfld = dynamic_cast<const field::Numeric<false>*>(fld);
    if (pfld == nullptr) {
        throw std::invalid_argument("Field " + fld->name() + " is not unsigned");
    }
    return pfld;
}

field::Field* create_varstring(const FieldToken& token, const structure::StructFormatter::FieldListT& fields) {
    const field::Numeric<false>* poffset = nullptr;
    const field::Numeric<false>* psize = nullptr;
    for (const auto& field : fields) {
        if (field->name() == token.field_offset) {
            poffset = get_numeric(field.get());
        } else if (field->name() == token.field_size) {
            psize = get_numeric(field.get());
        }
        if (poffset != nullptr && psize != nullptr) {
            break;
        }
    }
    if (poffset == nullptr) {
        throw std::invalid_argument("Field " + token.field_offset + " not found");
    }
    if (psize == nullptr) {
        throw std::invalid_argument("Field " + token.field_size + " not found");
    }
    return new field::VarString(token.name, poffset, psize);
}

structure::StructFormatter::FieldP itemize_field(const std::string& line,
      size_t offset,
      const structure::StructFormatter::FieldListT& fields) {
    const auto token = tokenize_field(line);
    switch (token.type) {
        case FieldType::Char:
            if (token.size == 1) {
                return std::unique_ptr<field::Field>(new field::Character(token.name, offset));
            } else {
                return std::unique_ptr<field::Field>(new field::String(token.name, offset, token.size));
            }
        case FieldType::Int:
            return std::unique_ptr<field::Field>(create_Int(token, offset));
        case FieldType::UInt:
            return std::unique_ptr<field::Field>(create_UInt(token, offset));
        case FieldType::Be:
            return std::unique_ptr<field::Field>(create_Be(token, offset));
        case FieldType::UBe:
            return std::unique_ptr<field::Field>(create_UBe(token, offset));
        case FieldType::IP:
            return std::unique_ptr<field::Field>(create_IP(token, offset));
        case FieldType::Hex:
            return std::unique_ptr<field::Field>(new field::Hex(token.name, offset, token.size));
        case FieldType::VarString:
            return std::unique_ptr<field::Field>(create_varstring(token, fields));
        case FieldType::UTC:
            return std::unique_ptr<field::Field>(create_UTC(token, offset));
    }
    throw std::logic_error("Builder::itemize_field - invalid token type - could not happen");
}

structure::Struct itemize_node(const std::string& line) {
    const auto token = tokenize_node(line);
    if (token.path != "buffer") {
        return structure::Struct(token.offset, token.name, Loader::load_struct(token.path));
    } else {
        if (token.name.substr(0, 7) == "buffer ") {
            size_t processed;
            int buffer_size = std::stoi(token.name.substr(7), &processed);
            if (processed == 0) {
                throw std::invalid_argument("unable to create node from line [" + line + "]");
            }
            return Loader::build_node(token.offset, buffer_size);
        } else {
            throw std::invalid_argument("unable to create node from line [" + line + "]");
        }
    }
}

void remove_comments_and_trim(std::string& line) {
    const auto pos_comment = line.find('#');
    if (pos_comment != std::string::npos) {
        line.erase(pos_comment);
    }

    util::rtrim(line);
}
}

structure::StructFormatter Loader::load_struct(const std::string& path) {
    using FieldListT = structure::StructFormatter::FieldListT;
    std::ifstream in(path);
    if (!in) {
        throw std::invalid_argument("could not open file " + path);
    }
    FieldListT fields;
    size_t offset = 0;
    std::string line;
    while (std::getline(in, line)) {
        remove_comments_and_trim(line);
        if (line.empty()) {
            continue;
        }
        fields.push_back(itemize_field(line, offset, fields));
        offset += fields.back()->static_size();
    }
    return structure::StructFormatter(path, std::move(fields));
}

structure::BatchFormatter Loader::load_batch(const std::string& path) {
    using NodeListT = structure::BatchFormatter::NodeListT;
    std::ifstream in(path);
    if (!in) {
        throw std::invalid_argument("could not open file " + path);
    }
    NodeListT nodes;
    std::string line;
    while (std::getline(in, line)) {
        remove_comments_and_trim(line);
        if (line.empty()) {
            continue;
        }
        nodes.push_back(itemize_node(line));
    }
    return structure::BatchFormatter(std::move(nodes));
}

structure::Struct Loader::build_node(const size_t offset, size_t size) {
    structure::StructFormatter::FieldListT fields;
    const std::string name = "buffer " + std::to_string(size);
    for (size_t inner_offset = 0; inner_offset < size; inner_offset += 8) {
        fields.emplace_back(new field::String("buffer", inner_offset, std::min(size - inner_offset, size_t(8))));
    }
    return structure::Struct(offset, name, structure::StructFormatter("buffer", std::move(fields)));
}

void Loader::save(const structure::BatchFormatter& batch, const std::string& path) {
    std::ofstream out(path);
    if (!out) {
        throw std::runtime_error("It was not possible to open " + path + " for writing");
    }

    size_t maximum_width = 0;
    for (const auto& node : batch.nodes()) {
        maximum_width = std::max(node.name().length(), maximum_width);
    }
    for (const auto& node : batch.nodes()) {
        out << std::right << std::setw(10) << node.offset()
            << " | " << std::left << std::setw(maximum_width) << node.name()
            << " | " << node.origin()
            << std::endl;
    }
}
}
