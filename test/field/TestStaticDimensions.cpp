
#include "field/StaticDimensions.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace field {

namespace {
class StaticDimensionsMock : public StaticDimensions {
public:
    StaticDimensionsMock(const std::string& name, size_t offset, size_t size)
          : StaticDimensions(name, offset, size) {
    }
    ~StaticDimensionsMock() noexcept override = default;
    MOCK_CONST_METHOD1(do_format, std::string(const uint8_t* buffer));
};
}

TEST(TestStaticDimensions, dimensions) {
    const uint8_t buffer[] = { 0x01 };
    const StaticDimensionsMock s1("s1", 21, 57);
    const StaticDimensionsMock s2("s2", 39, 48);
    EXPECT_EQ(s1.static_offset(), 21u);
    EXPECT_EQ(s2.static_offset(), 39u);
    EXPECT_EQ(s1.dynamic_offset(buffer), 21u);
    EXPECT_EQ(s2.dynamic_offset(buffer), 39u);
    EXPECT_EQ(s1.static_size(), 57u);
    EXPECT_EQ(s2.static_size(), 48u);
    EXPECT_EQ(s1.dynamic_size(buffer), 57u);
    EXPECT_EQ(s2.dynamic_size(buffer), 48u);
}

using ::testing::Return;

TEST(TestStaticDimensions, format) {
    const uint8_t buffer[] = { 's', 'o', 'm', 'e', ' ', 's', 't', 'r', 'i', 'n', 'g' };
    const StaticDimensionsMock mock("mock name", 1, 2);
    const Field& field = mock;
    EXPECT_CALL(mock, do_format(buffer)).WillOnce(Return("formatted string"));

    EXPECT_EQ(field.format(buffer), "formatted string");
}
}
