
#include "TestFieldUtil.h"

#include "field/IPv4.h"

#include <gtest/gtest.h>

namespace field {

TEST(TestIPv4, sizes) {
    const IPv4 ip("name", 792);
    EXPECT_EQ(ip.static_offset(), 792u);
    EXPECT_EQ(ip.dynamic_offset(TestUtil::ONE_BYTE), 792u);
    EXPECT_EQ(ip.static_size(), 4u);
    EXPECT_EQ(ip.dynamic_size(TestUtil::ONE_BYTE), 4u);
}

TEST(TestIPv4, format) {
    const uint8_t buffer[] = { 1, 2, 3, 4, 5, 6 };
    const IPv4 ip1("name", 0);
    const IPv4 ip2("name", 1);
    TestUtil::verifyFormat(ip1, buffer, "1.2.3.4");
    TestUtil::verifyFormat(ip1, buffer + 1, "2.3.4.5");
    TestUtil::verifyFormat(ip2, buffer, "2.3.4.5");
    TestUtil::verifyFormat(ip2, buffer + 1, "3.4.5.6");
}
}
