#pragma once

#include "field/Numeric.h"

#include <algorithm>
#include <string>

namespace field {

namespace impl {
template <bool is_signed, size_t offset_msb>
struct Initial {};

template <size_t offset_msb>
struct Initial<true, offset_msb> {
    static int64_t value(const uint8_t* buffer) {
        const uint8_t most_significant_byte = buffer[offset_msb];
        return (most_significant_byte & 0x80u) == 0 ? 0 : -1;
    }
};

template <size_t offset_msb>
struct Initial<false, offset_msb> {
    static uint64_t value(const uint8_t* /*buffer*/) {
        return 0;
    }
};

template <size_t num_bytes>
struct IntCopier {
    template <typename T>
    static T copy(const uint8_t* buffer, T value0) {
        auto val_buffer = reinterpret_cast<uint8_t*>(&value0);
        std::copy(buffer, buffer + num_bytes, val_buffer);
        return value0;
    }
};

template <size_t num_bytes>
struct BigEndianCopier {
    template <typename T>
    static T copy(const uint8_t* buffer, T value0) {
        auto val_buffer = reinterpret_cast<uint8_t*>(&value0);
        std::reverse_copy(buffer, buffer + num_bytes, val_buffer);
        return value0;
    }
};

template <bool is_signed, size_t num_bytes, typename Initial, typename Copier>
class BaseIntN : public Numeric<is_signed> {
public:
    static_assert(num_bytes > 0, "cannot be empty");
    static_assert(num_bytes <= 8, "at most 8 bytes");

    using Base = Numeric<is_signed>;
    using ValueT = typename Base::ValueT;

    BaseIntN(std::string name, const size_t offset)
          : Base(std::move(name), offset, num_bytes) {}

    ValueT value(const uint8_t* buffer) const override {
        const auto off = StaticDimensions::dynamic_offset(buffer);
        ValueT val = Initial::value(buffer + off);
        return Copier::copy(buffer + off, val);
    }

private:
    std::string do_format(const uint8_t* buffer) const override {
        return std::to_string(value(buffer));
    }
};
}

template <bool is_signed, size_t num_bytes>
class IntN : public impl::BaseIntN<is_signed, num_bytes, impl::Initial<is_signed, num_bytes - 1>, impl::IntCopier<num_bytes>> {
public:
    using Base = impl::BaseIntN<is_signed, num_bytes, impl::Initial<is_signed, num_bytes - 1>, impl::IntCopier<num_bytes>>;

    IntN(std::string name, const size_t offset)
          : Base(std::move(name), offset) {}
};

template <bool is_signed, size_t num_bytes>
class BigEndianN : public impl::BaseIntN<is_signed, num_bytes, impl::Initial<is_signed, 0>, impl::BigEndianCopier<num_bytes>> {
public:
    using Base = impl::BaseIntN<is_signed, num_bytes, impl::Initial<is_signed, 0>, impl::BigEndianCopier<num_bytes>>;

    BigEndianN(std::string name, const size_t offset)
          : Base(std::move(name), offset) {}
};
}
