
#include "driver/Driver.h"
#include "persistence/Loader.h"
#include "ui/Curses.h"
#include "ui/Degrade.h"
#include "ui/Rainbow.h"
#include "ui/Text.h"

#include <getopt.h>

#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>

namespace {
enum class UIType {
    TEXT,
    DEGRADE,
    RAINBOW,
    NCURSES,
};

std::unique_ptr<ui::UI> create_ui(const UIType type) {
    switch (type) {
        case UIType::TEXT:
            return std::unique_ptr<ui::UI>(new ui::Text(std::cin, std::cout, std::cerr));
        case UIType::DEGRADE:
            return std::unique_ptr<ui::UI>(new ui::Degrade(std::cin, std::cout, std::cerr));
        case UIType::RAINBOW:
            return std::unique_ptr<ui::UI>(new ui::Rainbow(std::cin, std::cout, std::cerr));
        case UIType::NCURSES:
            return std::unique_ptr<ui::UI>(new ui::Curses());
    }
    throw std::invalid_argument("create_ui - unknown type " + std::to_string(static_cast<int>(type)));
}

void usage(const char* app) {
    std::cerr << "Usage: " << ::basename(app)
              << " [-b batch_file] [-d data_file] [-TDRh]\n";
    std::cerr << "Options:\n"
                 "  -b, --batch=batch_file     batch file\n"
                 "  -d, --data=data_file       data file to interpret\n"
                 "  -T, --text                 select text interpreter (default interpreter)\n"
                 "  -D, --degrade              select degrade text interpreter\n"
                 "  -R, --rainbow              select rainbow text interpreter\n"
                 "  -C, --curses               select curses interpreter\n"
                 "  -h, --help                 display this message\n";
}

void opt_error(const char* app, const std::string& msg) {
    std::cerr << "ERROR in parameters: " << msg << std::endl;
    usage(app);
    exit(1);
}

void no_argument_error(const char* app, const int c) {
    opt_error(app, "option requires an argument -- '" + std::string(1, c) + "'");
}

void check_argument(const char* app,
      const std::string& argument,
      int c) {
    if (argument.empty() || argument[0] == '-') {
        no_argument_error(app, c);
    }
}

void process_parameters(int argc,
      char* argv[],
      std::string& batch_name,
      std::string& data_name,
      UIType& type) {
    struct option long_options[] = {
        { "batch", required_argument, nullptr, 'b' },
        { "data", required_argument, nullptr, 'd' },
        { "text", no_argument, nullptr, 'T' },
        { "degrade", no_argument, nullptr, 'D' },
        { "rainbow", no_argument, nullptr, 'R' },
        { "curses", no_argument, nullptr, 'C' },
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 },
    };
    int c;
    while ((c = getopt_long(argc, argv, "b:d:TDRCh", long_options, nullptr)) != -1) {
        switch (c) {
            case 'b':
                check_argument(argv[0], optarg, c);
                batch_name = optarg;
                break;
            case 'd':
                check_argument(argv[0], optarg, c);
                data_name = optarg;
                break;
            case 'T':
                type = UIType::TEXT;
                break;
            case 'D':
                type = UIType::DEGRADE;
                break;
            case 'R':
                type = UIType::RAINBOW;
                break;
            case 'C':
                type = UIType::NCURSES;
                break;
            case 'h':
                usage(argv[0]);
                exit(0);
            case '?':
                usage(argv[0]);
                exit(1);
            default:
                break;
        }
    }
}
}

int main(int argc, char* argv[]) {
    std::string batch_name, data_name;
    UIType type = UIType::NCURSES; // default interpreter
    process_parameters(argc, argv, batch_name, data_name, type);

    if (batch_name.empty()) {
        opt_error(argv[0], "batch file not informed (-b)");
    }

    if (data_name.empty()) {
        opt_error(argv[0], "data file not informed (-d)");
    }

    driver::Driver drv(batch_name, create_ui(type));
    try {
        std::ifstream data(data_name, std::ios::in | std::ios::binary);
        if (!data) {
            throw std::runtime_error("It was not possible to open " + data_name);
        }

        drv.run(data);
    } catch (const std::exception& e) {
        drv.show_exception("Error processing " + batch_name + " in " + data_name, e);
        return 1;
    }
    return 0;
}
