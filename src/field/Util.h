#pragma once

#include <endian.h>

#include <cstdint>

namespace field {
class Util {
public:
    static int8_t betoh(int8_t value) { return value; }
    static int16_t betoh(int16_t value) { return be16toh(static_cast<uint16_t>(value)); }
    static int32_t betoh(int32_t value) { return be32toh(static_cast<uint32_t>(value)); }
    static int64_t betoh(int64_t value) { return be64toh(static_cast<uint64_t>(value)); }
    static uint8_t betoh(uint8_t value) { return value; }
    static uint16_t betoh(uint16_t value) { return be16toh(value); }
    static uint32_t betoh(uint32_t value) { return be32toh(value); }
    static uint64_t betoh(uint64_t value) { return be64toh(value); }
};

template <typename FIELD>
struct Concatenator {
    explicit Concatenator(const FIELD& the_field)
          : m_field(the_field),
            result("") {
    }

    void operator()(const uint8_t& value) {
        result += m_field.format(&value);
    }

    const FIELD& m_field;
    std::string result;
};
}
