#include "Curses.h"
#include "util/Util.h"

#include <form.h>
#include <ncurses.h>
#include <unistd.h>

#include <cassert>

namespace ui {

namespace {

constexpr int HEIGHT = 50;
constexpr int WIDTH = 120;
constexpr int LEFT_MARGIN = 5;
constexpr int TOP_MARGIN = 2;

constexpr int Black = 0;
constexpr int Red = 1;
constexpr int Green = 2;
constexpr int Yellow = 3;
constexpr int Blue = 4;
constexpr int Magenta = 5;
constexpr int Cyan = 6;
constexpr int White = 7;
constexpr int Navy = 18;
constexpr int Orange = 130;
constexpr int LightOrange = 172;
constexpr int Roxo = 98;
constexpr int Lime = 46;
constexpr int BrightRed = 9;
constexpr int LightGray = 102;
constexpr int LightBlue = 32;
constexpr int LightRed = 175;

constexpr size_t LENGTH_NUMBER = 10;
constexpr size_t LENGTH_PATH = 80;
constexpr size_t LENGTH_NAME = 30;

enum ColorIdx {
    COLOR_IDX_HEX = 1,
    COLOR_IDX_NAME,
    COLOR_IDX_VALUE,
    COLOR_IDX_SIZE,
    COLOR_IDX_PROCESSING,
    COLOR_IDX_TEXT,
    COLOR_IDX_MENU_OPTION,
    COLOR_IDX_ERROR_MSG,
    COLOR_IDX_INPUT_BOX,
};

void init_colors() {
    init_pair(COLOR_IDX_HEX, Orange, Black);
    init_pair(COLOR_IDX_NAME, LightBlue, Black);
    init_pair(COLOR_IDX_VALUE, Lime, Black);
    init_pair(COLOR_IDX_SIZE, BrightRed, Black);
    init_pair(COLOR_IDX_PROCESSING, LightRed, Black);
    init_pair(COLOR_IDX_TEXT, Cyan, Black);
    init_pair(COLOR_IDX_MENU_OPTION, LightRed, Black);
    init_pair(COLOR_IDX_ERROR_MSG, Yellow, Red);
    init_pair(COLOR_IDX_INPUT_BOX, Yellow, Navy);
}

void show_menu(WINDOW* win,
      const std::string& section,
      const std::vector<std::pair<std::string, std::string>>& commands) {
    wattron(win, COLOR_PAIR(COLOR_IDX_TEXT));
    wprintw(win, "%s ", section.c_str());
    wattroff(win, COLOR_PAIR(COLOR_IDX_TEXT));
    bool first = true;
    for (const auto& command : commands) {
        if (not first) {
            wprintw(win, " / ");
        }

        wprintw(win, "'");
        wattron(win, COLOR_PAIR(COLOR_IDX_MENU_OPTION));
        wprintw(win, command.first.c_str());
        wattroff(win, COLOR_PAIR(COLOR_IDX_MENU_OPTION));
        wprintw(win, "' %s", command.second.c_str());
        first = false;
    }
}

void show_menu(WINDOW*) {
    WINDOW* win = newwin(7, WIDTH - 2, TOP_MARGIN + HEIGHT - 8, LEFT_MARGIN + 1);
    box(win, 0, 0);
    int line = 0;
    wmove(win, ++line, 1);
    show_menu(win, "general     ", {
                                         std::make_pair("q", "to quit"),
                                         std::make_pair("r", "to reload batch file"),
                                         std::make_pair("s", "to save batch file"),
                                   });
    wmove(win, ++line, 1);
    show_menu(win, "navigation  ", {
                                         std::make_pair("Up", "go to previous"),
                                         std::make_pair("Down", "go to next"),
                                         std::make_pair("Home", "go to first"),
                                         std::make_pair("End", "go to last"),
                                   });
    wmove(win, ++line, 1);
    show_menu(win, "            ", {
                                         std::make_pair("PgUp", "go to previous of same spec"),
                                         std::make_pair("PgDown", "go to next of same spec"),
                                   });
    wmove(win, ++line, 1);
    show_menu(win, "manipulation", {
                                         std::make_pair("i", "to create a new struct"),
                                         std::make_pair("b", "to add a buffer struct"),
                                         std::make_pair("d", "to delete current struct"),
                                   });

    wmove(win, ++line, 1);
    show_menu(win, "            ", {
                                         std::make_pair("f", "to fix offset"),
                                         std::make_pair("x", "to copy current to the end"),
                                   });

    wrefresh(win);
    delwin(win);
}

struct Input {
    Input(std::string label,
          std::string default_value,
          size_t length)
          : label(std::move(label)),
            default_value(std::move(default_value)),
            length(length) {
    }
    std::string label;
    std::string default_value;
    size_t length;
};

std::vector<std::string> dialog(WINDOW* topwindow, const std::vector<Input>& inputs) {
    assert(topwindow != nullptr);
    curs_set(1);
    constexpr int TOP_IN_WINDOW = 20;
    std::vector<FIELD*> fields(inputs.size() * 2 + 1, nullptr);
    for (size_t i = 0; i < inputs.size(); ++i) {
        const auto& input = inputs[i];
        FIELD* field = new_field(1, int(input.label.length()), int(i + 1), 1, 0, 0);
        assert(field != nullptr);
        set_field_back(field, COLOR_PAIR(COLOR_IDX_TEXT));
        set_field_fore(field, COLOR_PAIR(COLOR_IDX_TEXT));
        set_field_opts(field, O_VISIBLE | O_PUBLIC | O_AUTOSKIP);
        set_field_buffer(field, 0, input.label.c_str());
        fields.at(2 * i) = field;

        field = new_field(1, int(input.length), int(i + 1), int(input.label.size() + 2), 0, 0);
        assert(field != nullptr);
        set_field_back(field, COLOR_PAIR(COLOR_IDX_INPUT_BOX));
        set_field_fore(field, COLOR_PAIR(COLOR_IDX_INPUT_BOX));
        field_opts_off(field, O_AUTOSKIP | O_STATIC);
        set_field_buffer(field, 0, input.default_value.c_str());
        fields.at(2 * i + 1) = field;
    }

    WINDOW* winform = derwin(topwindow, int(inputs.size() + 2), WIDTH - 4, TOP_MARGIN + TOP_IN_WINDOW, 2);
    assert(winform != nullptr);
    FORM* form = new_form(fields.data());
    assert(form != nullptr);
    set_form_win(form, winform);
    post_form(form);
    box(winform, 0, 0);
    refresh();
    wrefresh(topwindow);
    for (size_t i = 0; i < inputs.size(); ++i) {
        form_driver(form, REQ_END_LINE);
        form_driver(form, REQ_NEXT_FIELD);
        form_driver(form, REQ_END_LINE);
    }
    wrefresh(winform);

    bool done = false;
    while (not done) {
        const int c = getch();
        switch (c) {
            case 10:
                if (field_index(current_field(form)) + 2 == (int) fields.size()) {
                    // Or the current field buffer won't be sync with what is displayed
                    form_driver(form, REQ_PREV_FIELD);
                    form_driver(form, REQ_NEXT_FIELD);

                    done = true;
                } else {
                    form_driver(form, REQ_NEXT_FIELD);
                    form_driver(form, REQ_END_LINE);
                }
                break;
            case KEY_DOWN:
                form_driver(form, REQ_NEXT_FIELD);
                form_driver(form, REQ_END_LINE);
                break;
            case KEY_UP:
                form_driver(form, REQ_PREV_FIELD);
                form_driver(form, REQ_END_LINE);
                break;
            case KEY_LEFT:
                form_driver(form, REQ_PREV_CHAR);
                break;
            case KEY_RIGHT:
                form_driver(form, REQ_NEXT_CHAR);
                break;
            case KEY_BACKSPACE:
            case 127:
                form_driver(form, REQ_DEL_PREV);
                break;
            case KEY_DC:
                form_driver(form, REQ_DEL_CHAR);
                break;
            default:
                form_driver(form, c);
                break;
        }

        wrefresh(winform);
    }

    std::vector<std::string> res;
    for (size_t i = 0; i < inputs.size(); ++i) {
        std::string resp = field_buffer(fields[2 * i + 1], 0);
        util::trim(resp);
        res.push_back(resp);
    }

    unpost_form(form);
    free_form(form);
    for (size_t i = 0; i < inputs.size() * 2; ++i) {
        free_field(fields[i]);
    }

    delwin(winform);

    curs_set(0);

    redrawwin(topwindow);
    return res;
}

std::vector<std::string> error_dialog(WINDOW* topwindow, const std::vector<std::string>& msgs) {
    constexpr int TOP_IN_WINDOW = 20;
    std::vector<std::string> res;
    WINDOW* win = newwin(int(msgs.size() + 3), WIDTH - 20, TOP_MARGIN + TOP_IN_WINDOW, LEFT_MARGIN + 10);
    wbkgd(win, COLOR_PAIR(COLOR_IDX_ERROR_MSG));
    box(win, 0, 0);
    size_t line = 0;
    for (const auto& msg : msgs) {
        mvwprintw(win, int(++line), 1, msg.c_str());
    }

    mvwprintw(win, int(++line), 1, "Press any key to continue...");
    wgetch(win);
    delwin(win);

    wredrawln(topwindow, TOP_IN_WINDOW, int(msgs.size() + 3));
    return res;
}

UI::NodeParameters get_parameters(WINDOW* win,
      const size_t offset_hint,
      const std::string& path_hint,
      const std::string& name_hint) {
    const auto hint = std::to_string(offset_hint);

    const auto resps = dialog(win, {
                                         Input("Enter offset in data file:", hint, LENGTH_NUMBER),
                                         Input("Enter file path:", path_hint, LENGTH_PATH),
                                         Input("Enter name:", name_hint, LENGTH_NAME),
                                   });

    const size_t offset = std::stoul(resps.at(0));
    return { offset, resps.at(2), resps.at(1) };
}
}

Curses::Curses()
      : UI(),
        m_win(nullptr) {
    initscr();
    start_color();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0);
    m_win = newwin(HEIGHT, WIDTH, TOP_MARGIN, LEFT_MARGIN);
    keypad(m_win, TRUE);
    init_colors();
    refresh();
}

Curses::~Curses() {
    clrtoeol();
    refresh();
    delwin(m_win);
    m_win = nullptr;
    endwin();
}

Operation Curses::get_input() const {
    int c = wgetch(m_win);

    switch (c) {
        case KEY_UP: return Operation::Previous;
        case KEY_DOWN: return Operation::Next;
        case KEY_PPAGE: return Operation::PreviousSame;
        case KEY_NPAGE: return Operation::NextSame;
        case KEY_HOME: return Operation::First;
        case KEY_END: return Operation::Last;
        case 'Q':
        case 'q': return Operation::Quit;
        case 'R':
        case 'r': return Operation::Reload;
        case 'S':
        case 's': return Operation::Save;
        case 'D':
        case 'd': return Operation::Delete;
        case 'I':
        case 'i': return Operation::New;
        case 'B':
        case 'b': return Operation::AddBuffer;
        case 'F':
        case 'f': return Operation::Fix;
        case 'X':
        case 'x': return Operation::Duplicate;
        case '?':
            show_menu(m_win);
            break;
        default:
            break;
    }
    return Operation::NoOp;
}

void Curses::show(const structure::BatchFormatter& batch,
      const size_t index,
      std::istream& data) {
    wclear(m_win);
    box(m_win, 0, 0);

    try {
        const auto& node = batch.nodes().at(index);
        const auto formatted = batch.format(index, data);
        const auto maximum_name_size = structure::StructFormatter::maximum_name_size(formatted.fields);

        const std::string page = std::to_string(index + 1) + "/" + std::to_string(batch.nodes().size());
        mvwprintw(m_win, 1, int(WIDTH - 1 - page.length()), "%s", page.c_str());

        int line = 0;
        // print origin[...]
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        mvwprintw(m_win, ++line, 1, "spec path [");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_PROCESSING));
        wprintw(m_win, node.origin().c_str());
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_PROCESSING));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wprintw(m_win, "]");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));

        // print processing[...]
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        mvwprintw(m_win, ++line, 1, "name      [");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_PROCESSING));
        wprintw(m_win, node.name().c_str());
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_PROCESSING));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wprintw(m_win, "]");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));

        // print offset and size
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        mvwprintw(m_win, ++line, 1, "offset ");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
        wprintw(m_win, "%zu", node.offset());
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wprintw(m_win, " / size ");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
        wprintw(m_win, "%zu", formatted.buffer.size());
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_SIZE));

        ++line;
        // print data
        for (size_t i = 0; i < formatted.fields.size(); ++i) {
            ++line;
            int col = 2;
            const auto& formatted_field = formatted.fields[i];

            wattron(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
            mvwprintw(m_win, line, col, "%4zu (%zu)", formatted_field.offset, formatted_field.size);
            wattroff(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
            col += 11;
            mvwprintw(m_win, line, col, "-");

            col += 2;
            wattron(m_win, COLOR_PAIR(COLOR_IDX_HEX));
            mvwprintw(m_win, line, col, "%s", format_hex(formatted.buffer, formatted_field, 8).c_str());
            wattroff(m_win, COLOR_PAIR(COLOR_IDX_HEX));

            col += 28;
            mvwprintw(m_win, line, col, "|");

            col += 2;
            wattron(m_win, COLOR_PAIR(COLOR_IDX_NAME));
            mvwprintw(m_win, line, col, "%s", formatted_field.name.c_str());
            wattroff(m_win, COLOR_PAIR(COLOR_IDX_NAME));
            mvwprintw(m_win, line, col + maximum_name_size + 1, "=");
            if (formatted_field.valid) {
                wattron(m_win, COLOR_PAIR(COLOR_IDX_VALUE));
            } else {
                wattron(m_win, COLOR_PAIR(COLOR_IDX_ERROR_MSG));
            }
            mvwprintw(m_win, line, col + maximum_name_size + 3, "%s", formatted_field.value.c_str());
            wattroff(m_win, COLOR_PAIR(COLOR_IDX_VALUE));
        }

        ++line;
        // print next offset
        wattron(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        mvwprintw(m_win, ++line, 1, "next offset ");
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_TEXT));
        wattron(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
        wprintw(m_win, "%zu", node.offset() + formatted.buffer.size());
        wattroff(m_win, COLOR_PAIR(COLOR_IDX_SIZE));
    } catch (std::exception& e) {
        data.clear();
        show_exception_and_wait_confirm("unable to show struct " + std::to_string(index), e);
    }
    wrefresh(m_win);
}

UI::NodeParameters Curses::get_node_parameters(const structure::BatchFormatter& batch, size_t index) const {
    const auto offset_hint = (index < batch.nodes().size())
                                   ? batch.nodes()[index].offset() + batch.nodes()[index].formatter().static_size()
                                   : 0;

    return get_parameters(m_win, offset_hint, "", "");
}

UI::NodeParameters Curses::get_fixed_parameters(const structure::BatchFormatter& batch, size_t index) const {
    const auto& node = batch.nodes().at(index);
    return get_parameters(m_win, node.offset(), node.origin(), node.name());
}

int Curses::get_buffer_size() const {
    const auto resps = dialog(m_win, {
                                           Input("Enter buffer size:", "", LENGTH_NUMBER),
                                     });

    return std::stoi(resps.at(0));
}

std::string Curses::get_batch_name(const std::string& current_name) const {
    const auto resps = dialog(m_win, {
                                           Input("Enter batch path:", current_name, LENGTH_PATH),
                                     });

    return resps.at(0);
}

void Curses::show_exception_and_wait_confirm(const std::string& msg, const std::exception& e) const {
    error_dialog(m_win, { msg, e.what() });
}

void Curses::wait_confirm(const std::string&) const {
    printw("Press any key to continue...");
    wgetch(m_win);
}

bool Curses::want_to_save_first() const {
    const auto resps = dialog(m_win, {
                                           Input("Want to save batch (Y/N)?", "Y", 2),
                                     });
    return resps[0] == "Y" || resps[0] == "y";
}
}
