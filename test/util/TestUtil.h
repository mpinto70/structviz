
#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace util {
class TestUtil {
public:
    template <size_t N>
    static std::vector<uint8_t> convert_array(const uint8_t (&content)[N]) {
        return std::vector<uint8_t>(content, content + N);
    }
    static std::string create_text_file();
    static std::string create_binary_file();
    static std::string create_text_file(const std::string& content);
    static std::string create_binary_file(const std::vector<uint8_t>& content);
    template <size_t N>
    static std::string create_binary_file(const uint8_t (&content)[N]) {
        return create_binary_file(convert_array(content));
    }
    static void append_text(std::ostream& out, const std::string& content);
    static void append_binary(std::ostream& out, const std::vector<uint8_t>& content);
    static void append_binary(std::ostream& out, const std::string& content);
    template <size_t N>
    static void append_binary(std::ostream& out, const uint8_t (&content)[N]) {
        append_binary(out, convert_array<N>(content));
    }
    static const std::vector<std::string>& files_created() { return m_files_created; }
    static void clear_files() { m_files_created.clear(); }

private:
    static std::vector<std::string> m_files_created;

    static std::string create_tmp_file(const std::string& base_name);
};
}
