
#include "../test/field/TestFieldUtil.h"
#include "../test/persistence/TestPersistenceUtil.h"
#include "../test/structure/TestStructureUtil.h"
#include "TestUIUtil.h"

#include "ui/Text.h"

#include <gtest/gtest.h>

#include <errno.h>
#include <fstream>
#include <sstream>

namespace ui {

namespace {
void verify_get_input(const std::map<char, Operation>& operation_map) {
    std::istringstream in;
    std::ostringstream out;
    std::ostringstream err;
    Text text(in, out, err);
    for (const auto& it : operation_map) {
        out.str("");
        in.str(std::string(1, it.first));
        EXPECT_EQ(text.get_input(), it.second) << "for [" << it.first << "]";
        EXPECT_EQ(out.str(), "");
        EXPECT_EQ(err.str(), "");
    }
}
}

TEST(TestText, get_input) {
    std::map<char, Operation> operation_map;
    for (char c = 'a'; c <= 'z'; ++c) {
        operation_map[c] = Operation::NoOp;
    }
    operation_map['q'] = Operation::Quit;
    operation_map['s'] = Operation::Save;
    operation_map['r'] = Operation::Reload;
    operation_map['d'] = Operation::Delete;
    operation_map['i'] = Operation::New;
    operation_map['b'] = Operation::AddBuffer;
    operation_map['x'] = Operation::Duplicate;

    verify_get_input(operation_map);
}

namespace {
void verify_get_input_navigation(const std::map<char, Operation>& operation_map) {
    std::istringstream in;
    std::ostringstream out;
    std::ostringstream err;
    Text text(in, out, err);
    for (const auto& it : operation_map) {
        out.str("");
        in.str("\033[" + std::string(1, it.first));
        EXPECT_EQ(text.get_input(), Operation::NoOp) << "for [" << it.first << "]";
        EXPECT_EQ(text.get_input(), Operation::NoOp) << "for [" << it.first << "]";
        EXPECT_EQ(text.get_input(), it.second) << "for [" << it.first << "]";
        EXPECT_EQ(out.str(), "");
        EXPECT_EQ(err.str(), "");
    }
}
}

TEST(TestText, get_input_navigation) {
    std::map<char, Operation> operation_map;
    for (char c = 'a'; c <= 'z'; ++c) {
        operation_map[c] = Operation::NoOp;
    }
    operation_map['A'] = Operation::Previous;
    operation_map['B'] = Operation::Next;
    operation_map['1'] = Operation::First;
    operation_map['4'] = Operation::Last;

    verify_get_input_navigation(operation_map);
}

namespace {
std::string build_output(const structure::BatchFormatter& batch,
      const size_t index,
      const std::string& content) {
    const auto& node = batch.nodes()[index];

    std::ostringstream out;
    out << std::string(50, '-') << "\n";
    out << Text::LightOrange;
    out << "processing [" << Text::BCyan << node.name() << Text::LightOrange << "]\n";
    out << Text::LightRed;
    out << "offset " << Text::BRed << node.offset() << Text::LightRed << " / size " << Text::BRed << node.formatter().static_size() << "\n";
    out << Text::Lime;
    out << content;
    out << Text::LightRed;
    out << "next offset " << Text::BRed << node.offset() + node.formatter().static_size() << Text::LightRed << "\n";
    out << Text::RESET;
    out << std::string(50, '-') << "\n";
    return out.str();
}
}

TEST(TestText, print) {
    structure::BatchFormatter batch{ structure::TestUtil::create_batch() };
    std::string filename{ persistence::TestUtil::create_content_file_batch() };
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    ASSERT_TRUE(in);

    std::ostringstream out;
    std::ostringstream err;
    Text text(in, out, err);

    text.show(batch, 0, in);
    EXPECT_EQ(out.str(), build_output(batch, 0, TestUtil::INT_CONTENT_FOR_BUFFER));
    EXPECT_EQ(err.str(), "");

    out.str("");

    text.show(batch, 1, in);
    EXPECT_EQ(out.str(), build_output(batch, 1, TestUtil::BE_CONTENT_FOR_BUFFER));
    EXPECT_EQ(err.str(), "");

    out.str("");

    text.show(batch, 2, in);
    EXPECT_EQ(out.str(), build_output(batch, 2, TestUtil::BE_CONTENT_FOR_BUFFER));
    EXPECT_EQ(err.str(), "");
}

namespace {
void verify_output_get_node_parameters(const structure::BatchFormatter& batch,
      const size_t index,
      const std::string& output) {
    const auto& nodes = batch.nodes();
    const size_t hint = index < nodes.size() ? nodes[index].offset() + nodes[index].formatter().static_size() : 0;
    const std::string str_out = "Enter offset in data file (" + std::to_string(hint) + "): "
                                + "Enter file full path: "
                                + "Enter descriptive name: ";
    EXPECT_EQ(output, str_out) << index;
}
}

TEST(TestText, get_node_parameters) {
    structure::BatchFormatter batch{ structure::TestUtil::create_batch() };

    UI::NodeParameters params;
    std::ostringstream out;
    std::ostringstream err;
    std::istringstream in;
    Text text(in, out, err);

    out.str("");
    in.str("123\npath\nname\n");
    params = text.get_node_parameters(batch, 0);
    EXPECT_EQ(params.offset, 123u);
    EXPECT_EQ(params.path, "path");
    EXPECT_EQ(params.name, "name");
    EXPECT_EQ(err.str(), "");
    verify_output_get_node_parameters(batch, 0, out.str());

    out.str("");
    in.str("456\nPATH\nNAME\n");
    params = text.get_node_parameters(batch, 1);
    EXPECT_EQ(params.offset, 456u);
    EXPECT_EQ(params.path, "PATH");
    EXPECT_EQ(params.name, "NAME");
    EXPECT_EQ(err.str(), "");
    verify_output_get_node_parameters(batch, 1, out.str());
}

TEST(TestText, get_buffer_size) {
    const std::string output = "Enter buffer size: ";
    size_t size;
    std::ostringstream out;
    std::ostringstream err;
    std::istringstream in;
    Text text(in, out, err);

    out.str("");
    in.str("730\n");
    size = text.get_buffer_size();
    EXPECT_EQ(size, 730u);
    EXPECT_EQ(out.str(), output);
    EXPECT_EQ(err.str(), "");

    out.str("");
    in.str("1258\n");
    size = text.get_buffer_size();
    EXPECT_EQ(size, 1258u);
    EXPECT_EQ(out.str(), output);
    EXPECT_EQ(err.str(), "");
}

TEST(TestText, get_batch_name) {
    const std::string output = "Enter file name to save: ";
    std::string name;
    std::ostringstream out;
    std::ostringstream err;
    std::istringstream in;
    Text text(in, out, err);

    out.str("");
    in.str("some name\n");
    name = text.get_batch_name("default name");
    EXPECT_EQ(name, "some name");
    EXPECT_EQ(out.str(), output);
    EXPECT_EQ(err.str(), "");

    out.str("");
    in.str("other name\n");
    name = text.get_batch_name("default name");
    EXPECT_EQ(name, "other name");
    EXPECT_EQ(out.str(), output);
    EXPECT_EQ(err.str(), "");

    out.str("");
    in.str("\n");
    name = text.get_batch_name("default name");
    EXPECT_EQ(name, "default name");
    EXPECT_EQ(out.str(), output);
    EXPECT_EQ(err.str(), "");
}

namespace {
void verify_show_exception_and_wait_confirm(const std::string& msg, const std::string& exception_msg) {
    std::ostringstream err;
    std::ostringstream out;
    std::istringstream in;
    Text text(in, out, err);

    std::runtime_error error(exception_msg);

    in.str("x");
    text.show_exception_and_wait_confirm(msg, error);

    const std::string err_out = std::string(Text::BYellow) + Text::OnRed + msg + Text::RESET + "\n"
                                + Text::BYellow + Text::OnRed + exception_msg + Text::RESET + "\n";

    EXPECT_EQ(out.str(), "\nPress any key to continue...\n");
    EXPECT_EQ(err.str(), err_out);
    EXPECT_EQ(in.tellg(), 1u);
}
}

TEST(TestText, show_exception_and_wait_confirm) {
    verify_show_exception_and_wait_confirm("some message", "some exception");
    verify_show_exception_and_wait_confirm("other message", "other exception");
}

namespace {
void verify_wait_confirm(const std::string& msg) {
    std::ostringstream err;
    std::ostringstream out;
    std::istringstream in;
    Text text(in, out, err);

    in.str("x");
    text.wait_confirm(msg);

    EXPECT_EQ(out.str(), msg + "\nPress any key to continue...\n");
    EXPECT_EQ(err.str(), "");
    EXPECT_EQ(in.tellg(), 1u);
}
}

TEST(TestText, wait_confirm) {
    verify_wait_confirm("some message");
    verify_wait_confirm("other message");
}
}
