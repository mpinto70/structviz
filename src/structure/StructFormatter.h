#pragma once

#include "field/Field.h"

#include <memory>
#include <vector>

namespace structure {

struct FormattedField {
    FormattedField(std::string name,
          size_t offset,
          size_t size,
          std::string value)
          : name(std::move(name)),
            offset(offset),
            size(size),
            value(std::move(value)),
            valid(true) {
    }
    explicit FormattedField(std::string name)
          : name(std::move(name)),
            offset(0),
            size(0),
            value("ERROR READING"),
            valid(false) {
    }
    const std::string name;
    const size_t offset;
    const size_t size;
    const std::string value;
    const bool valid;
};

class StructFormatter {
public:
    using FieldP = std::unique_ptr<field::Field>;
    using FieldListT = std::vector<FieldP>;
    using FormattedFieldListT = std::vector<FormattedField>;

    StructFormatter(std::string origin, FieldListT&& fields);

    FormattedFieldListT format(const uint8_t* buffer, size_t buffer_size) const;
    size_t static_size() const { return m_size; }
    size_t dynamic_size(const uint8_t* buffer) const;
    const std::string& origin() const { return m_origin; }
    const FieldListT& fields() const { return m_fields; }
    static size_t maximum_name_size(const FormattedFieldListT& items);

private:
    std::string m_origin;
    FieldListT m_fields;
    size_t m_size;
};
}
