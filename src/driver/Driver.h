#pragma once

#include "structure/BatchFormatter.h"
#include "ui/UI.h"

#include <istream>
#include <memory>

namespace driver {

class Driver {
public:
    Driver(const std::string& batch_name, std::unique_ptr<ui::UI>&& ui);
    void run(std::istream& data);
    void show_exception(const std::string& msg, const std::exception& e) const;
    const structure::BatchFormatter& batch() const { return m_batch; }

private:
    std::string m_batch_name;
    structure::BatchFormatter m_batch;
    std::unique_ptr<ui::UI> m_ui;
    size_t m_index;
    bool m_changed;

    bool process(ui::Operation operation, std::istream& data);
    void insert_node(structure::Struct&& node);
    structure::Struct create_node(size_t index) const;
    void replace_node(structure::Struct&& node);
    structure::Struct fix_node(size_t index) const;
    structure::Struct create_buffer(size_t index) const;
    void duplicate_node_at_end();
    void save_batch() const;
    void position_same_origin(bool forward);
};
}
