
#pragma once
#include "structure/BatchFormatter.h"
#include "structure/StructFormatter.h"

#include <string>
#include <vector>

namespace structure {
class TestUtil {
public:
    static StructFormatter::FieldListT UINT_field_list_for_BUFFER();
    static StructFormatter::FieldListT BE_field_list_for_BUFFER();

    static void verifyFormatted(const StructFormatter::FormattedFieldListT& formatted,
          const std::vector<std::pair<size_t, size_t>>& offsets_sizes,
          const std::vector<std::string>& names,
          const std::vector<std::string>& values);
    static void verifyFormat(const StructFormatter& strct,
          const uint8_t* buffer,
          size_t buffer_size,
          const std::vector<std::pair<size_t, size_t>>& offsets_sizes,
          const std::vector<std::string>& names,
          const std::vector<std::string>& values);

    static BatchFormatter create_batch();
    static std::vector<uint8_t> create_content_for_batch();
};
}
