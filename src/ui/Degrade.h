#pragma once

#include "ui/Text.h"

namespace ui {

class Degrade : public Text {
public:
    Degrade(std::istream& in,
          std::ostream& out,
          std::ostream& err);
    void show(const structure::BatchFormatter& batch,
          size_t index,
          std::istream& data) override;

private:
    std::ostream& out;
};
}
