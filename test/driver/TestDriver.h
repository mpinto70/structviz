#pragma once

#include "gtest/gtest.h"
#include <istream>
#include <memory>
#include <string>

namespace driver {

class MockUI;
class TestDriver : public ::testing::Test {
protected:
    void SetUp() override;
    void TearDown() override;

    std::unique_ptr<MockUI> pmock;
    std::unique_ptr<std::istream> data;
    std::string batch_name;
};
}
