#include "Hex.h"
#include "Util.h"
#include "util/Util.h"

#include <algorithm>

namespace field {
Hex::Hex(std::string name,
      const size_t offset,
      const size_t size)
      : StaticDimensions(std::move(name), offset, size) {
}

struct ToHex {
    static constexpr char hex_values[] = "0123456789abcdef";
    std::string format(const uint8_t* value) const {
        std::string result(3, ' ');
        result[0] = hex_values[(*value & 0xf0u) >> 4u];
        result[1] = hex_values[*value & 0x0fu];
        return result;
    }
};

std::string Hex::do_format(const uint8_t* buffer) const {
    ToHex hex_field;
    const auto off = dynamic_offset(buffer);
    const auto siz = static_size();
    auto concatenated = std::for_each(&buffer[off], &buffer[off + siz], Concatenator<ToHex>(hex_field));
    return util::rtrim(concatenated.result);
}
}
