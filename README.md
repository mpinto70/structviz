# structviz

This is a simple struct visualizer for helping interpret binary files.

1. Spec files are pipe (|) separated files. Empty lines are ignored. Content after a # is a comment and is ignored

2. The types currently supported for struct members are (case sensitive):
 - **char** (single char and string. If not printable, will output a dot)
 - **int** (8, 16, 32, 64 - interprets the content as native signed integer)
 - **uint** (8, 16, 32, 64 - interprets the content as native unsigned integer)
 - **be** (8, 16, 32, 64 - interprets the content as Big Endian signed integer, useful for networked content)
 - **ube** (8, 16, 32, 64 - interprets the content as Big Endian unsigned integer, useful for networked content)
 - **IP** (IP address. For IPv4 specify size **4**. IPv6 not being treated yet)
 - **hex** (like char, but outputs the hex number)
 - **varstring** (variable size string. It is necessary to specify the fields where offset and size are found)
 - **utc** (timestamps that will be interpreted as seconds or nanoseconds since epoch and converted to YYYY-MM-DD HH:MM:SS[.nnnnnnnnn])

3. The file to specify a struct is a | separeted file composed of:
 - type: one of the above
 - size: size of field
   - for char and hex, number of characters
   - for int and be, number of bits
   - for IP, 4 for IPv4
   - for varstring the name of the offset field **comma** the name of the size field
   - for utc, number of bits (32 for time in seconds and 64 for time in nanoseconds)
 - name: name to display

 Ex.:
 ```
 char      | 8                    | misc info
 be        | 8                    | protocol
 be        | 16                   | checksum
 IP        | 4                    | source
 IP        | 4                    | destination
 IP        | 4                    | destination
 uint      | 8                    | str offset
 uint      | 8                    | str size
 utc       | 32                   | date/time in seconds
 utc       | 64                   | date/time in nanoseconds
 varstring | str offset,str size  | str
 ```

4. The file to specify a batch is a | separeted file composed of:
 - offset: offset in binary file where struct start
 - name: an identifier to be shown
 - struct spec path: path to struct specification file (specified in 3)

 Ex.:
 ```
 0   | PCAP Header         | specs/pcap/pcapHeader.struct
 24  | PCAP Packet Header  | specs/pcap/pcapPacketHeader.struct
 ```

5. To run the program:
 ```
 structviz -b <batch file> -d <data file to be interpreted> [-<T|D|R|C|h>]
 ```

 Ex.:
 ```
 structviz -b specs/pcap.batch -d pcap.dump -C
 ```
