#pragma once

#include "field/StaticDimensions.h"

namespace field {

class Hex : public StaticDimensions {
public:
    Hex(std::string name,
          size_t offset,
          size_t size);
    ~Hex() noexcept override = default;

private:
    std::string do_format(const uint8_t* buffer) const override;
};
}
