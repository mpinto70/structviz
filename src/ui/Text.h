#pragma once

#include "UI.h"

namespace ui {

class Text : public UI {
public:
    constexpr static char Black[] = "\033[0;30m";
    constexpr static char Red[] = "\033[0;31m";
    constexpr static char Green[] = "\033[0;32m";
    constexpr static char Yellow[] = "\033[0;33m";
    constexpr static char Blue[] = "\033[0;34m";
    constexpr static char Purple[] = "\033[0;35m";
    constexpr static char Cyan[] = "\033[0;36m";
    constexpr static char White[] = "\033[0;37m";
    constexpr static char BBlack[] = "\033[1;30m";
    constexpr static char BRed[] = "\033[1;31m";
    constexpr static char BGreen[] = "\033[1;32m";
    constexpr static char BYellow[] = "\033[1;33m";
    constexpr static char BBlue[] = "\033[1;34m";
    constexpr static char BPurple[] = "\033[1;35m";
    constexpr static char BCyan[] = "\033[1;36m";
    constexpr static char BWhite[] = "\033[1;37m";
    constexpr static char UBlack[] = "\033[4;30m";
    constexpr static char URed[] = "\033[4;31m";
    constexpr static char UGreen[] = "\033[4;32m";
    constexpr static char UYellow[] = "\033[4;33m";
    constexpr static char UBlue[] = "\033[4;34m";
    constexpr static char UPurple[] = "\033[4;35m";
    constexpr static char UCyan[] = "\033[4;36m";
    constexpr static char UWhite[] = "\033[4;37m";
    constexpr static char IBlack[] = "\033[0;90m";
    constexpr static char IRed[] = "\033[0;91m";
    constexpr static char IGreen[] = "\033[0;92m";
    constexpr static char IYellow[] = "\033[0;93m";
    constexpr static char IBlue[] = "\033[0;94m";
    constexpr static char IPurple[] = "\033[0;95m";
    constexpr static char ICyan[] = "\033[0;96m";
    constexpr static char IWhite[] = "\033[0;97m";
    constexpr static char BIBlack[] = "\033[1;90m";
    constexpr static char BIRed[] = "\033[1;91m";
    constexpr static char BIGreen[] = "\033[1;92m";
    constexpr static char BIYellow[] = "\033[1;93m";
    constexpr static char BIBlue[] = "\033[1;94m";
    constexpr static char BIPurple[] = "\033[1;95m";
    constexpr static char BICyan[] = "\033[1;96m";
    constexpr static char BIWhite[] = "\033[1;97m";
    constexpr static char OnBlack[] = "\033[40m";
    constexpr static char OnRed[] = "\033[41m";
    constexpr static char OnGreen[] = "\033[42m";
    constexpr static char OnYellow[] = "\033[43m";
    constexpr static char OnBlue[] = "\033[44m";
    constexpr static char OnPurple[] = "\033[45m";
    constexpr static char OnCyan[] = "\033[46m";
    constexpr static char OnWhite[] = "\033[47m";
    constexpr static char Orange[] = "\033[38;5;130m";
    constexpr static char LightOrange[] = "\033[1;38;5;172m";
    constexpr static char Roxo[] = "\033[38;5;98m";
    constexpr static char OnClearWhite[] = "\033[48;5;15m";
    constexpr static char OnDGreen[] = "\033[48;5;22m";
    constexpr static char Lime[] = "\033[38;5;46m";
    constexpr static char BrightRed[] = "\033[38;5;9m";
    constexpr static char LightGray[] = "\033[38;5;102m";
    constexpr static char LightBlue[] = "\033[38;5;30m";
    constexpr static char LightRed[] = "\033[38;5;175m";

    constexpr static char RESET[] = "\033[0m";

    Text(std::istream& in, std::ostream& out, std::ostream& err);
    ~Text() noexcept override = default;
    Operation get_input() const override;
    void show(const structure::BatchFormatter& batch, size_t index, std::istream& data) override;
    NodeParameters get_node_parameters(const structure::BatchFormatter& batch, size_t index) const override;
    NodeParameters get_fixed_parameters(const structure::BatchFormatter& batch, size_t index) const override;
    int get_buffer_size() const override;
    std::string get_batch_name(const std::string& current_name) const override;
    void show_exception_and_wait_confirm(const std::string& msg, const std::exception& e) const override;
    void wait_confirm(const std::string& msg) const override;
    bool want_to_save_first() const override;
    static std::string format(const structure::FormattedStruct& formatted);

private:
    std::istream& in;
    std::ostream& out;
    std::ostream& err;
};
}
