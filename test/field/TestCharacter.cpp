
#include "TestFieldUtil.h"

#include "field/Character.h"

#include <gtest/gtest.h>

namespace field {

TEST(TestCharacter, sizes) {
    const Character c("name", 127u);
    EXPECT_EQ(c.static_offset(), 127u);
    EXPECT_EQ(c.dynamic_offset(TestUtil::ONE_BYTE), 127u);
    EXPECT_EQ(c.static_size(), 1u);
    EXPECT_EQ(c.dynamic_size(TestUtil::ONE_BYTE), 1u);
}

TEST(TestCharacter, format) {
    const uint8_t buffer[] = { 'b', 'u', 'f', 'f', 'e', 'r', 0x02 };
    const Character c1("name", 0);
    const Character c2("name", 1);
    TestUtil::verifyFormat(c1, buffer, "b");
    TestUtil::verifyFormat(c1, buffer + 1, "u");
    TestUtil::verifyFormat(c1, buffer + 6, ".");
    TestUtil::verifyFormat(c2, buffer, "u");
    TestUtil::verifyFormat(c2, buffer + 1, "f");
    TestUtil::verifyFormat(c2, buffer + 5, ".");
}
}
