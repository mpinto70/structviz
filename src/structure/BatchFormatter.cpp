#include "BatchFormatter.h"

#include <stdexcept>

namespace structure {
Struct::Struct(size_t offset,
      const std::string& name,
      StructFormatter&& formatter)
      : m_offset(offset),
        m_name(name),
        m_formatter(std::move(formatter)) {
}

BatchFormatter::BatchFormatter(NodeListT&& nodes)
      : m_nodes(std::move(nodes)) {
}

void BatchFormatter::remove(const size_t index) {
    if (index >= m_nodes.size()) {
        throw std::out_of_range("BatchFormatter::remove: index out of range " + std::to_string(index) + "/" + std::to_string(m_nodes.size()));
    }
    m_nodes.erase(m_nodes.begin() + index);
}

FormattedStruct BatchFormatter::format(const size_t index, std::istream& in) const {
    if (index >= m_nodes.size()) {
        throw std::out_of_range("BatchFormatter::format(1): index out of range "
                                + std::to_string(index)
                                + "/" + std::to_string(m_nodes.size()));
    }
    const auto& node = m_nodes[index];
    std::vector<uint8_t> buffer;
    buffer.reserve(node.formatter().static_size() + 100);
    const auto& formatter = node.formatter();
    in.seekg(node.offset());
    if (!in) {
        throw std::runtime_error("BatchFormatter::format(2): not possible to seek to "
                                 + std::to_string(node.offset())
                                 + " at index " + std::to_string(index));
    }
    const size_t static_size = formatter.static_size();
    buffer.resize(static_size);
    in.read((char*) &buffer[0], static_size);
    if (in.eof() && in.gcount() < static_cast<int>(static_size)) {
        buffer.resize(in.gcount());
        in.clear();
    } else {
        const size_t dynamic_size = formatter.dynamic_size(buffer.data());
        if (dynamic_size > static_size) {
            const size_t additional = dynamic_size - static_size;
            buffer.resize(dynamic_size);
            in.read((char*) &buffer[static_size], additional);
            if (in.eof() && in.gcount() < static_cast<int>(additional)) {
                buffer.resize(static_size + in.gcount());
                in.clear();
            }
        }
    }

    return { formatter.format(buffer.data(), buffer.size()), buffer };
}
}
