#pragma once

#include "field/Character.h"
#include "field/StaticDimensions.h"

namespace field {
class String : public StaticDimensions {
public:
    String(std::string name,
          size_t offset,
          size_t size);
    ~String() noexcept override = default;

private:
    const Character m_character;

    std::string do_format(const uint8_t* buffer) const override;
};
}
