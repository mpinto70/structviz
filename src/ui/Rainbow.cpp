#include "Rainbow.h"

#include <algorithm>
#include <random>

namespace ui {
namespace {

std::string color(const size_t i) {
    std::string res = "\033[1;38;5;" + std::to_string(i % 256) + "m"
                      + "\033[48;5;" + std::to_string((i + 127) % 256) + "m";
    return res;
}

void print_rainbow(std::ostream& out, const std::string& text) {
    std::mt19937 rng{ std::random_device()() };
    std::uniform_int_distribution<std::mt19937::result_type> dist(0, 255);

    auto output_f = [&out, &rng, &dist](char c) {
        if (c == '\n') {
            out << Text::RESET << '\n';
        } else {
            out << color(dist(rng)) << c;
        }
    };
    std::for_each(text.begin(), text.end(), output_f);
    out << Text::RESET;
    out.flush();
}
}

Rainbow::Rainbow(std::istream& in,
      std::ostream& out,
      std::ostream& err)
      : Text(in, out, err),
        out(out) {
}

void Rainbow::show(const structure::BatchFormatter& batch,
      size_t index,
      std::istream& data) {
    try {
        const auto formatted = batch.format(index, data);
        const auto& node = batch.nodes().at(index);

        out << std::string(50, '-') << "\n";
        out << LightBlue;
        out << "processing [" << LightOrange << node.name() << LightBlue << "]\n";
        out << "offset " << LightOrange << node.offset() << LightBlue << "\n";
        out << Lime;
        print_rainbow(out, format(formatted));
        out << LightRed;
        out << "next offset " << BRed << node.offset() + formatted.buffer.size() << LightRed << "\n";
        out << RESET;
        out << std::string(50, '-') << "\n";
    } catch (std::exception& e) {
        data.clear();
        show_exception_and_wait_confirm("unable to show struct " + std::to_string(index), e);
    }
}
}
