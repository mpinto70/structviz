
#include "TestFieldUtil.h"

#include "field/Hex.h"

#include <gtest/gtest.h>

namespace field {

TEST(TestHex, sizes) {
    const Hex s1("s1", 37, 1);
    const Hex s2("s2", 49, 2);
    EXPECT_EQ(s1.static_offset(), 37u);
    EXPECT_EQ(s2.static_offset(), 49u);
    EXPECT_EQ(s1.dynamic_offset(TestUtil::ONE_BYTE), 37u);
    EXPECT_EQ(s2.dynamic_offset(TestUtil::ONE_BYTE), 49u);
    EXPECT_EQ(s1.static_size(), 1u);
    EXPECT_EQ(s2.static_size(), 2u);
    EXPECT_EQ(s1.dynamic_size(TestUtil::ONE_BYTE), 1u);
    EXPECT_EQ(s2.dynamic_size(TestUtil::ONE_BYTE), 2u);
}

TEST(TestHex, format) {
    const uint8_t buffer[] = { 0x01, 0x02, 0x03, 0x56, 0x57, 0x58, 0x59 };
    const Hex s1("s1", 0, 1);
    const Hex s2("s2", 1, 2);
    TestUtil::verifyFormat(s1, buffer, "01");
    TestUtil::verifyFormat(s2, buffer, "02 03");
    TestUtil::verifyFormat(s1, buffer + 1, "02");
    TestUtil::verifyFormat(s2, buffer + 1, "03 56");
    TestUtil::verifyFormat(s1, buffer + 4, "57");
    TestUtil::verifyFormat(s2, buffer + 4, "58 59");
}
}
