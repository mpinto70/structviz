#pragma once

#include <string>

namespace util {
// taken from stack overflow (http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring)
// trim from start (in place)
std::string& ltrim(std::string& s);
// trim from end (in place)
std::string& rtrim(std::string& s);
// trim from both ends (in place)
std::string& trim(std::string& s);
}
