#include "Field.h"

namespace field {

Field::Field(std::string name)
      : m_name(std::move(name)) {
}

std::string Field::format(const uint8_t* buffer) const {
    return do_format(buffer);
}

bool Field::is_valid(const uint8_t* buffer, const size_t buffer_size) const {
    return can_compute_offset_size(buffer, buffer_size)
           && buffer_size >= dynamic_offset(buffer) + dynamic_size(buffer);
}
}
