#pragma once

#include "structure/BatchFormatter.h"

#include <string>

namespace persistence {

class Loader {
public:
    static structure::StructFormatter load_struct(const std::string& path);
    static structure::BatchFormatter load_batch(const std::string& path);
    static structure::Struct build_node(size_t offset, size_t size);
    static void save(const structure::BatchFormatter& batch, const std::string& path);
};
}
