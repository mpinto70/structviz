
#pragma once
#include "field/Field.h"
#include "field/Numeric.h"
#include "structure/BatchFormatter.h"
#include "structure/StructFormatter.h"

#include <gmock/gmock.h>

#include <string>
#include <vector>

namespace field {
class TestUtil {
public:
    static void verifyFormat(const Field& field,
          const uint8_t* buffer,
          const std::string& output);

    static constexpr uint8_t ONE_BYTE[] = { 0x01 };
    static constexpr uint8_t BUFFER[] = {
        // clang-format off
        'M', 'A', 'R', 'C', 'E', 'L', 'O', ' ', 'P', 'I', 'N', 'T', 'O',
        0x00, 0x01, 0x02, 0x03, 0x04, 0x20, 0x06, 0x07,
        'H', 'H',
        0x02, 0x01,
        0x11, 0x22, 0x33, 0x44,
        0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        'G', 'A', 'R', 'B', 'A', 'G', 'E',
        // clang-format on
    };

    static const std::vector<std::pair<size_t, size_t>> INT_OFFSETS_SIZES_FOR_BUFFER;
    static const std::vector<std::pair<size_t, size_t>> BE_OFFSETS_SIZES_FOR_BUFFER;
    static const std::vector<std::string> INT_NAMES_FOR_BUFFER;
    static const std::vector<std::string> INT_VALUES_FOR_BUFFER;
    static const std::vector<std::string> UINT_VALUES_FOR_BUFFER;
    static const std::vector<std::string> BE_NAMES_FOR_BUFFER;
    static const std::vector<std::string> BE_VALUES_FOR_BUFFER;
    static const std::vector<std::string> UBE_VALUES_FOR_BUFFER;

    static constexpr size_t INT_SIZE = 37;
    static constexpr size_t BE_SIZE = 44;

    static constexpr uint8_t ALL_TYPES_BUFFER[] = {
        // clang-format off
            // string and char
            'M', 'A', 'R', 'C', 'E', 'L', 'O', ' ', 'P', 'I', 'N', 'T', 'O',
            'C',
            // int and uint
            0xA2,                                           // -94
            0x37, 0xA2,                                     // -24009
            0xE0, 0x37, 0xA2,                               // -6146080
            0x32, 0xE0, 0x37, 0xA2,                         // -1573396430
            0xBC, 0x32, 0xE0, 0x37, 0xA2,                   // -402789485892
            0xA2, 0xBC, 0x32, 0xE0, 0x37, 0xA2,             // -103114108388190
            0x98, 0xA2, 0xBC, 0x32, 0xE0, 0x37, 0xA2,       // -26397211747376488
            0x01, 0x98, 0xA2, 0xBC, 0x32, 0xE0, 0x37, 0xA2, // -6757686207328380927
            0xA2,                                           // 162
            0x37, 0xA2,                                     // 41527
            0xE0, 0x37, 0xA2,                               // 10631136
            0x32, 0xE0, 0x37, 0xA2,                         // 2721570866
            0xBC, 0x32, 0xE0, 0x37, 0xA2,                   // 696722141884
            0xA2, 0xBC, 0x32, 0xE0, 0x37, 0xA2,             // 178360868322466
            0x98, 0xA2, 0xBC, 0x32, 0xE0, 0x37, 0xA2,       // 45660382290551448
            0x01, 0x98, 0xA2, 0xBC, 0x32, 0xE0, 0x37, 0xA2, // 11689057866381170689
            // be and ube
            0xA2,                                           // -94
            0xA2, 0x37,                                     // -24009
            0xA2, 0x37, 0xE0,                               // -6146080
            0xA2, 0x37, 0xE0, 0x32,                         // -1573396430
            0xA2, 0x37, 0xE0, 0x32, 0xBC,                   // -402789485892
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2,             // -103114108388190
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2, 0x98,       // -26397211747376488
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2, 0x98, 0x01, // -6757686207328380927
            0xA2,                                           // 162
            0xA2, 0x37,                                     // 41527
            0xA2, 0x37, 0xE0,                               // 10631136
            0xA2, 0x37, 0xE0, 0x32,                         // 2721570866
            0xA2, 0x37, 0xE0, 0x32, 0xBC,                   // 696722141884
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2,             // 178360868322466
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2, 0x98,       // 45660382290551448
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2, 0x98, 0x01, // 11689057866381170689
            // IP
            0xC0, 0xA8, 0x02, 0x01,                         // 192.168.2.1
            // hex (14)
            0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2, 0x98, 0x01, 0xA2, 0x37, 0xE0, 0x32, 0xBC, 0xA2,
            // UTC
            0x66, 0xD4, 0xC0, 0x35,                         // 1998-07-30 20:15:34
            0xB1, 0xE4, 0xD2, 0xA9, 0x5D, 0xF1, 0x83, 0x0C, // 1998-07-30 20:15:34.987654321
            // varstring
            0x07, 0x00,                                     // 7 characters
            0xC2, 0x00, 0x00, 0x00,                         // offset of 194 from the beginning of the buffer
            'A', 'l', 'm', 'e', 'i', 'd', 'a'
        // clang-format on
    };
    static const std::vector<std::pair<size_t, size_t>> ALL_TYPES_OFFSETS_SIZES_FOR_BUFFER;
    static const std::vector<std::string> ALL_TYPES_NAMES_FOR_BUFFER;
    static const std::vector<std::string> ALL_TYPES_VALUES_FOR_BUFFER;
};

class UnsignedMock : public Numeric<false> {
public:
    UnsignedMock(const std::string& name, size_t offset, size_t size)
          : Numeric(name, offset, size) {
    }
    ~UnsignedMock() noexcept override = default;
    MOCK_CONST_METHOD1(value, uint64_t(const uint8_t* buffer));
    MOCK_CONST_METHOD1(do_format, std::string(const uint8_t* buffer));
};
}
