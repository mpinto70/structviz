#include "Text.h"

#include <iomanip>
#include <iostream>

namespace ui {

namespace {
int get_char_no_enter(std::istream& in) {
    if (system("stty cbreak -echo") == -1) {
        throw std::runtime_error("unable to prepare read key");
    }
    int c = in.get();
    if (system("stty cooked echo") == -1) {
        throw std::runtime_error("unable to revert read key");
    }
    return c;
}

void do_show_exception(const std::string& msg,
      const std::exception& e,
      std::ostream& err) {
    err << Text::BYellow << Text::OnRed << msg << Text::RESET << std::endl;
    err << Text::BYellow << Text::OnRed << e.what() << Text::RESET << std::endl;
}

void show_menu(std::ostream& out,
      const std::string& section,
      const std::vector<std::pair<std::string, std::string>>& commands) {
    out << Text::BWhite << section << Text::RESET << ": ";
    bool first = true;
    for (const auto& command : commands) {
        if (not first) {
            out << " / ";
        }
        out << "'" << Text::BCyan << command.first << Text::RESET << "' " << command.second;
        first = false;
    }
    out << '\n';
}

void show_menu(std::ostream& out) {
    show_menu(out, "general", {
                                    std::make_pair("q", "to quit"),
                                    std::make_pair("r", "to reload batch file"),
                                    std::make_pair("s", "to save batch file"),
                              });
    show_menu(out, "navigation", {
                                       std::make_pair("Up", "go to previous"),
                                       std::make_pair("Down", "go to next"),
                                       std::make_pair("Home", "go to first"),
                                       std::make_pair("End", "go to last"),
                                 });
    show_menu(out, "manipulation", {
                                         std::make_pair("i", "to create a new struct"),
                                         std::make_pair("b", "to add a buffer struct"),
                                         std::make_pair("d", "to delete current struct"),
                                   });
}

UI::NodeParameters get_node(const size_t hint,
      std::ostream& out,
      std::istream& in) {
    out << "Enter offset in data file (" << hint << "): ";
    size_t offset;
    in >> offset;
    in.ignore();
    out << "Enter file full path: ";
    std::string path;
    getline(in, path);
    std::string name;
    out << "Enter descriptive name: ";
    getline(in, name);
    return { offset, name, path };
}
}

constexpr char Text::Black[];
constexpr char Text::Red[];
constexpr char Text::Green[];
constexpr char Text::Yellow[];
constexpr char Text::Blue[];
constexpr char Text::Purple[];
constexpr char Text::Cyan[];
constexpr char Text::White[];
constexpr char Text::BBlack[];
constexpr char Text::BRed[];
constexpr char Text::BGreen[];
constexpr char Text::BYellow[];
constexpr char Text::BBlue[];
constexpr char Text::BPurple[];
constexpr char Text::BCyan[];
constexpr char Text::BWhite[];
constexpr char Text::UBlack[];
constexpr char Text::URed[];
constexpr char Text::UGreen[];
constexpr char Text::UYellow[];
constexpr char Text::UBlue[];
constexpr char Text::UPurple[];
constexpr char Text::UCyan[];
constexpr char Text::UWhite[];
constexpr char Text::IBlack[];
constexpr char Text::IRed[];
constexpr char Text::IGreen[];
constexpr char Text::IYellow[];
constexpr char Text::IBlue[];
constexpr char Text::IPurple[];
constexpr char Text::ICyan[];
constexpr char Text::IWhite[];
constexpr char Text::BIBlack[];
constexpr char Text::BIRed[];
constexpr char Text::BIGreen[];
constexpr char Text::BIYellow[];
constexpr char Text::BIBlue[];
constexpr char Text::BIPurple[];
constexpr char Text::BICyan[];
constexpr char Text::BIWhite[];
constexpr char Text::OnBlack[];
constexpr char Text::OnRed[];
constexpr char Text::OnGreen[];
constexpr char Text::OnYellow[];
constexpr char Text::OnBlue[];
constexpr char Text::OnPurple[];
constexpr char Text::OnCyan[];
constexpr char Text::OnWhite[];
constexpr char Text::Orange[];
constexpr char Text::LightOrange[];
constexpr char Text::Roxo[];
constexpr char Text::OnClearWhite[];
constexpr char Text::OnDGreen[];
constexpr char Text::Lime[];
constexpr char Text::BrightRed[];
constexpr char Text::LightGray[];
constexpr char Text::LightBlue[];
constexpr char Text::LightRed[];

constexpr char Text::RESET[];

Text::Text(std::istream& in_,
      std::ostream& out_,
      std::ostream& err_)
      : UI(),
        in(in_),
        out(out_),
        err(err_) {
}

Operation Text::get_input() const {
    static int is_esc = false;
    const int c = get_char_no_enter(in);
    if (c == '\033') {
        is_esc = 1;
        return Operation::NoOp;
    }
    if (is_esc == 1) {
        if (c == '[') {
            is_esc = 2;
            return Operation::NoOp;
        } else {
            is_esc = 0;
        }
    } else if (is_esc == 2) {
        is_esc = 0;
        switch (c) {
            default: return Operation::NoOp;
            case 'A':
                return Operation::Previous; // \033 [A ===> up arrow
            case 'B':
                return Operation::Next; // \033 [B ===> down arrow
            case '1':
                return Operation::First; // \033 [1 ===> home
            case '4':
                return Operation::Last; // \033 [4 ===> end
        }
    }
    switch (c) {
        default: return Operation::NoOp;
        case 'q': return Operation::Quit;
        case 's': return Operation::Save;
        case 'r': return Operation::Reload;
        case 'd': return Operation::Delete;
        case 'i': return Operation::New;
        case 'b': return Operation::AddBuffer;
        case 'x': return Operation::Duplicate;
        case '?':
            show_menu(out);
            return Operation::NoOp;
    }
}

void Text::show(const structure::BatchFormatter& batch,
      const size_t index,
      std::istream& data) {
    try {
        const auto formatted = batch.format(index, data);
        const auto& node = batch.nodes().at(index);

        out << std::string(50, '-') << "\n";
        out << LightOrange;
        out << "processing [" << BCyan << node.name() << LightOrange << "]\n";
        out << LightRed;
        out << "offset " << BRed << node.offset() << LightRed << " / size " << BRed << formatted.buffer.size() << "\n";
        out << Lime;
        out << format(formatted);
        out << LightRed;
        out << "next offset " << BRed << node.offset() + formatted.buffer.size() << LightRed << "\n";
        out << RESET;
        out << std::string(50, '-') << "\n";
    } catch (std::exception& e) {
        data.clear();
        do_show_exception("unable to show struct " + std::to_string(index), e, err);
    }
}

UI::NodeParameters Text::get_node_parameters(const structure::BatchFormatter& batch, size_t index) const {
    const size_t hint = index < batch.nodes().size()
                              ? batch.nodes()[index].offset() + batch.nodes()[index].formatter().static_size()
                              : 0;
    return get_node(hint, out, in);
}

UI::NodeParameters Text::get_fixed_parameters(const structure::BatchFormatter& batch, size_t index) const {
    return get_node(batch.nodes().at(index).offset(), out, in);
}

int Text::get_buffer_size() const {
    out << "Enter buffer size: ";
    int size;
    in >> size;
    in.ignore();
    return size;
}

std::string Text::get_batch_name(const std::string& current_name) const {
    out << "Enter file name to save: ";
    std::string path;
    std::getline(in, path);
    if (path.empty()) {
        return current_name;
    } else {
        return path;
    }
}

void Text::show_exception_and_wait_confirm(const std::string& msg, const std::exception& e) const {
    do_show_exception(msg, e, err);
    wait_confirm("");
}

void Text::wait_confirm(const std::string& msg) const {
    out << msg << std::endl
        << "Press any key to continue..." << std::endl;
    get_char_no_enter(in);
}

bool Text::want_to_save_first() const {
    out << "Do you want to save (Y/N)" << std::endl;
    const int c = get_char_no_enter(in);
    return c == 'Y' || c == 'y';
}

std::string Text::format(const structure::FormattedStruct& formatted) {
    const size_t maximum_name_size = structure::StructFormatter::maximum_name_size(formatted.fields);
    std::ostringstream out;
    const auto flags = out.flags();
    for (const auto& fld : formatted.fields) {
        out.flags(flags);
        const auto& name = fld.name;
        const auto& value = fld.value;
        const auto hex = format_hex(formatted.buffer, fld, 8);
        out << std::setfill(' ') << std::setw(3) << fld.offset << " - ";
        out << std::left << std::setfill(' ') << std::setw(27) << hex << " | ";

        out << std::left << std::setfill(' ') << std::setw(int(maximum_name_size)) << name << " = ";
        out << value << std::endl;
    }
    return out.str();
}
}
