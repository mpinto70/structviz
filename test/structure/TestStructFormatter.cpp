
#include "../test/field/TestFieldUtil.h"
#include "../test/structure/TestStructureUtil.h"

#include "structure/StructFormatter.h"

#include <gtest/gtest.h>

#include <memory>

namespace structure {

TEST(TestStructFormatter, convert_int) {
    const StructFormatter strct("origin", structure::TestUtil::UINT_field_list_for_BUFFER());
    EXPECT_EQ(strct.static_size(), field::TestUtil::INT_SIZE);
    EXPECT_EQ(strct.dynamic_size(field::TestUtil::ONE_BYTE), field::TestUtil::INT_SIZE);
    EXPECT_EQ(strct.origin(), "origin");

    structure::TestUtil::verifyFormat(strct,
          field::TestUtil::BUFFER,
          sizeof(field::TestUtil::BUFFER),
          field::TestUtil::INT_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::INT_NAMES_FOR_BUFFER,
          field::TestUtil::UINT_VALUES_FOR_BUFFER);
}

TEST(TestStructFormatter, convert_bigendian) {
    const StructFormatter strct("ORIGIN", structure::TestUtil::BE_field_list_for_BUFFER());
    EXPECT_EQ(strct.static_size(), field::TestUtil::BE_SIZE);
    EXPECT_EQ(strct.dynamic_size(field::TestUtil::ONE_BYTE), field::TestUtil::BE_SIZE);
    EXPECT_EQ(strct.origin(), "ORIGIN");

    structure::TestUtil::verifyFormat(strct,
          field::TestUtil::BUFFER,
          sizeof(field::TestUtil::BUFFER),
          field::TestUtil::BE_OFFSETS_SIZES_FOR_BUFFER,
          field::TestUtil::BE_NAMES_FOR_BUFFER,
          field::TestUtil::BE_VALUES_FOR_BUFFER);
}
}
