#include "Degrade.h"
#include <algorithm>

namespace ui {
namespace {

std::string color(const size_t i) {
    size_t cor = ((i / 3) % (256 - 232)) + 232;
    if (cor == 15 || cor > 242) {
        return std::string(Text::Black) + "\033[48;5;" + std::to_string(cor) + "m";
    } else {
        return std::string(Text::White) + "\033[48;5;" + std::to_string(cor) + "m";
    }
}

void print_degrade(std::ostream& out, const std::string& text) {
    size_t column = 0;
    auto output_f = [&out, &column](char c) {
        if (c == '\n') {
            out << Text::RESET;
            out << std::endl;
            column = 0;
        } else {
            out << color(column++) << c;
        }
    };
    std::for_each(text.begin(), text.end(), output_f);
    out << Text::RESET;
}
}

Degrade::Degrade(std::istream& in,
      std::ostream& out,
      std::ostream& err)
      : Text(in, out, err),
        out(out) {
}

void Degrade::show(const structure::BatchFormatter& batch,
      const size_t index,
      std::istream& data) {
    try {
        const auto formatted = batch.format(index, data);
        const auto& node = batch.nodes().at(index);

        out << std::string(50, '-') << "\n";
        out << LightBlue;
        out << "processing [" << LightOrange << node.name() << LightBlue << "]\n";
        out << "offset " << LightOrange << node.offset() << LightBlue << "\n";
        out << Lime;
        print_degrade(out, format(formatted));
        out << LightRed;
        out << "next offset " << BRed << node.offset() + formatted.buffer.size() << LightRed << "\n";
        out << RESET;
        out << std::string(50, '-') << "\n";
    } catch (std::exception& e) {
        data.clear();
        show_exception_and_wait_confirm("unable to show struct " + std::to_string(index), e);
    }
}
}
