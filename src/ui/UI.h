#pragma once

#include "structure/BatchFormatter.h"

#include <istream>

namespace ui {

enum class Operation {
    NoOp,
    Previous,
    Next,
    PreviousSame,
    NextSame,
    First,
    Last,
    Quit,
    Save,
    Reload,
    Delete,
    New,
    Fix,
    AddBuffer,
    Duplicate,
};

class UI {
public:
    struct NodeParameters {
        size_t offset;
        std::string name;
        std::string path;
    };

    virtual ~UI() noexcept = default;
    virtual Operation get_input() const = 0;
    virtual void show(const structure::BatchFormatter& batch,
          size_t index,
          std::istream& data) = 0;
    virtual NodeParameters get_node_parameters(const structure::BatchFormatter& batch, size_t index) const = 0;
    virtual NodeParameters get_fixed_parameters(const structure::BatchFormatter& batch, size_t index) const = 0;
    virtual int get_buffer_size() const = 0;
    virtual std::string get_batch_name(const std::string& current_name) const = 0;
    virtual void show_exception_and_wait_confirm(const std::string& msg, const std::exception& e) const = 0;
    virtual void wait_confirm(const std::string& msg) const = 0;
    virtual bool want_to_save_first() const = 0;
    static std::string format_hex(const std::vector<uint8_t>& buffer,
          const structure::FormattedField& field,
          size_t max_bytes);
};
}
