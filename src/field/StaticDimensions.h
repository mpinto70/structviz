#pragma once

#include "field/Field.h"

namespace field {

class StaticDimensions : public Field {
public:
    StaticDimensions(std::string name,
          size_t offset,
          size_t size)
          : Field(std::move(name)),
            m_offset(offset),
            m_size(size) {
    }
    ~StaticDimensions() noexcept override = default;
    size_t static_offset() const override { return m_offset; }
    size_t dynamic_offset(const uint8_t*) const override { return m_offset; }
    size_t static_size() const override { return m_size; }
    size_t dynamic_size(const uint8_t*) const override { return m_size; }

private:
    const size_t m_offset;
    const size_t m_size;
    bool can_compute_offset_size(const uint8_t*, size_t) const override {
        return true;
    }
};
}
