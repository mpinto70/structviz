
#include "TestFieldUtil.h"

#include "field/String.h"

#include <gtest/gtest.h>

namespace field {

TEST(TestString, sizes) {
    const String s10("s1", 29, 10);
    const String s20("s2", 77, 20);
    EXPECT_EQ(s10.static_offset(), 29u);
    EXPECT_EQ(s20.static_offset(), 77u);
    EXPECT_EQ(s10.dynamic_offset(TestUtil::ONE_BYTE), 29u);
    EXPECT_EQ(s20.dynamic_offset(TestUtil::ONE_BYTE), 77u);
    EXPECT_EQ(s10.static_size(), 10u);
    EXPECT_EQ(s20.static_size(), 20u);
    EXPECT_EQ(s10.dynamic_size(TestUtil::ONE_BYTE), 10u);
    EXPECT_EQ(s20.dynamic_size(TestUtil::ONE_BYTE), 20u);
}

TEST(TestString, format) {
    const uint8_t buffer[] = { 'b', 'u', 'f', 'f', 'e', 'r', '-', 0x02 };
    const String s1("s1", 0, 1);
    const String s2("s2", 1, 2);
    TestUtil::verifyFormat(s1, buffer, "b");
    TestUtil::verifyFormat(s2, buffer, "uf");
    TestUtil::verifyFormat(s1, buffer + 1, "u");
    TestUtil::verifyFormat(s2, buffer + 1, "ff");
    TestUtil::verifyFormat(s1, buffer + 5, "r");
    TestUtil::verifyFormat(s2, buffer + 5, "-.");
}
}
