#include "String.h"
#include "field/Util.h"

#include <algorithm>

namespace field {
String::String(std::string name,
      size_t offset,
      size_t size)
      : StaticDimensions(std::move(name), offset, size),
        m_character("for String", 0) {
}

std::string String::do_format(const uint8_t* buffer) const {
    const size_t off = dynamic_offset(buffer);
    const size_t siz = static_size();

    auto concatenated = std::for_each(&buffer[off], &buffer[off + siz], Concatenator<Character>(m_character));
    return concatenated.result;
}
}
