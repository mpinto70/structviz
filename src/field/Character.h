#pragma once

#include "field/StaticDimensions.h"

namespace field {
class Character : public StaticDimensions {
public:
    Character(std::string name, size_t offset);
    ~Character() noexcept override = default;

private:
    std::string do_format(const uint8_t* buffer) const override;
};
}
