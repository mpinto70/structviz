#include "UI.h"

#include <algorithm>
#include <iomanip>
#include <sstream>

namespace ui {
std::string UI::format_hex(const std::vector<uint8_t>& buffer,
      const structure::FormattedField& field,
      size_t max_bytes) {
    std::ostringstream out;
    const size_t end = std::min<size_t>(field.size, max_bytes);
    out << std::hex;
    for (size_t i = 0; i < end; ++i) {
        out << std::setfill('0') << std::setw(2) << unsigned(buffer[field.offset + i]) << ' ';
    }
    if (field.size > max_bytes) {
        out << "...";
    }
    return out.str();
}
}
