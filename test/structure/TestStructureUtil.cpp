
#include "TestStructureUtil.h"
#include "../test/field/TestFieldUtil.h"

#include "field/Character.h"
#include "field/IntN.h"
#include "field/String.h"

#include "gtest/gtest.h"

#include <fstream>

namespace structure {

StructFormatter::FieldListT TestUtil::UINT_field_list_for_BUFFER() {
    StructFormatter::FieldListT fields;
    fields.emplace_back(new field::String("name", 0, 13));
    fields.emplace_back(new field::String("eight", 13, 8));
    fields.emplace_back(new field::Character("char", 21));
    fields.emplace_back(new field::IntN<false, 1>("value8", 22));
    fields.emplace_back(new field::IntN<false, 2>("value16", 23));
    fields.emplace_back(new field::IntN<false, 4>("value32", 25));
    fields.emplace_back(new field::IntN<false, 8>("value64", 29));
    return fields;
}

StructFormatter::FieldListT TestUtil::BE_field_list_for_BUFFER() {
    StructFormatter::FieldListT fields;
    fields.emplace_back(new field::String("name", 0, 13));
    fields.emplace_back(new field::String("eight", 13, 8));
    fields.emplace_back(new field::Character("char", 21));
    fields.emplace_back(new field::BigEndianN<true, 1>("value8", 22));
    fields.emplace_back(new field::BigEndianN<true, 2>("value16", 23));
    fields.emplace_back(new field::BigEndianN<true, 4>("value32", 25));
    fields.emplace_back(new field::BigEndianN<true, 8>("value64", 29));
    fields.emplace_back(new field::String("garbage", 37, 7));
    return fields;
}

void TestUtil::verifyFormatted(const StructFormatter::FormattedFieldListT& formatted,
      const std::vector<std::pair<size_t, size_t>>& offsets_sizes,
      const std::vector<std::string>& names,
      const std::vector<std::string>& values) {
    EXPECT_EQ(names.size(), formatted.size());
    EXPECT_EQ(values.size(), formatted.size());

    for (size_t i = 0; i < formatted.size(); ++i) {
        const auto& f = formatted[i];
        EXPECT_EQ(f.offset, offsets_sizes.at(i).first);
        EXPECT_EQ(f.size, offsets_sizes.at(i).second);
        EXPECT_EQ(f.name, names.at(i));
        EXPECT_EQ(f.value, values.at(i));
    }
}
void TestUtil::verifyFormat(const StructFormatter& formatter,
      const uint8_t* buffer,
      const size_t buffer_size,
      const std::vector<std::pair<size_t, size_t>>& offsets_sizes,
      const std::vector<std::string>& names,
      const std::vector<std::string>& values) {
    const auto formatted = formatter.format(buffer, buffer_size);
    verifyFormatted(formatted, offsets_sizes, names, values);
}

BatchFormatter TestUtil::create_batch() {
    BatchFormatter batch(BatchFormatter::NodeListT{});
    batch.nodes().emplace_back(1 + 0 * sizeof(field::TestUtil::BUFFER), "first", StructFormatter("", UINT_field_list_for_BUFFER()));
    batch.nodes().emplace_back(2 + 1 * sizeof(field::TestUtil::BUFFER), "second", StructFormatter("", BE_field_list_for_BUFFER()));
    batch.nodes().emplace_back(2 + 2 * sizeof(field::TestUtil::BUFFER), "third", StructFormatter("", BE_field_list_for_BUFFER()));
    return batch;
}

std::vector<uint8_t> TestUtil::create_content_for_batch() {
    std::vector<uint8_t> res;
    res.reserve(sizeof(field::TestUtil::BUFFER) * 3 + 2);
    const std::vector<uint8_t> buffer(field::TestUtil::BUFFER, field::TestUtil::BUFFER + sizeof(field::TestUtil::BUFFER));
    res.push_back(0xFF);
    res.insert(res.end(), buffer.begin(), buffer.end());
    res.push_back(0x00);
    res.insert(res.end(), buffer.begin(), buffer.end());
    res.insert(res.end(), buffer.begin(), buffer.end());
    return res;
}
}
