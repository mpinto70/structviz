
#include "TestFieldUtil.h"

#include "field/UTC.h"

#include <gtest/gtest.h>

namespace field {

TEST(TestUTC, sizes) {
    const UTC32 utc32("utc 32", 49);
    const UTC64 utc64("utc 64", 37);
    EXPECT_EQ(utc32.static_offset(), 49u);
    EXPECT_EQ(utc64.static_offset(), 37u);
    EXPECT_EQ(utc32.dynamic_offset(TestUtil::ONE_BYTE), 49u);
    EXPECT_EQ(utc64.dynamic_offset(TestUtil::ONE_BYTE), 37u);
    EXPECT_EQ(utc32.static_size(), 4u);
    EXPECT_EQ(utc64.static_size(), 8u);
    EXPECT_EQ(utc32.dynamic_size(TestUtil::ONE_BYTE), 4u);
    EXPECT_EQ(utc64.dynamic_size(TestUtil::ONE_BYTE), 8u);
}

TEST(TestUTC, format) {
    uint8_t buffer[4 + 8];
    auto dt32 = reinterpret_cast<int32_t*>(buffer);
    auto dt64 = reinterpret_cast<int64_t*>(buffer + 4);
    *dt32 = 1517706220;
    *dt64 = 1517763819012345678;

    const UTC32 utc32("utc 32", 0);
    const UTC64 utc64("utc 64", 4);

    TestUtil::verifyFormat(utc32, buffer, "2018-02-04 01:03:40");
    TestUtil::verifyFormat(utc64, buffer, "2018-02-04 17:03:39.012345678");
}
}
