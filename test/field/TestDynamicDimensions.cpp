
#include "TestFieldUtil.h"

#include "field/DynamicDimensions.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace field {

namespace {
class DynamicDimensionsMock : public DynamicDimensions {
public:
    DynamicDimensionsMock(const std::string& name,
          const Numeric<false>* offset_field,
          const Numeric<false>* size_field)
          : DynamicDimensions(name, offset_field, size_field) {
    }
    ~DynamicDimensionsMock() noexcept override = default;
    MOCK_CONST_METHOD1(do_format, std::string(const uint8_t* buffer));
};
}

using ::testing::Return;

TEST(TestDynamicDimensions, dimensions) {
    const uint8_t buffer[] = { 's', 'o', 'm', 'e', ' ', 's', 't', 'r', 'i', 'n', 'g' };
    const UnsignedMock offset_field("offset_field", 1, 2);
    const UnsignedMock size_field("size_field", 3, 4);
    DynamicDimensionsMock mock("dynamic dimensions", &offset_field, &size_field);

    EXPECT_CALL(offset_field, value(buffer)).WillOnce(Return(3));
    EXPECT_CALL(size_field, value(buffer)).WillOnce(Return(4));

    const Field& field = mock;
    EXPECT_EQ(field.static_offset(), 0u);
    EXPECT_EQ(field.dynamic_offset(buffer), 3u);
    EXPECT_EQ(field.static_size(), 0u);
    EXPECT_EQ(field.dynamic_size(buffer), 4u);
}
}
