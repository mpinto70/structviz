
#include "TestFieldUtil.h"

#include "field/IntN.h"

#include <gtest/gtest.h>

namespace field {

namespace {
constexpr uint8_t buffer[] = { 0x8A, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0, 0xAA };
constexpr uint8_t ff[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
}

TEST(TestIntN, size) {
    const IntN<false, 1> vu8("vu", 90);
    const IntN<false, 2> vu16("vu", 100);
    const IntN<false, 3> vu24("vu", 110);
    const IntN<false, 4> vu32("vu", 120);
    const IntN<false, 5> vu40("vu", 130);
    const IntN<false, 6> vu48("vu", 140);
    const IntN<false, 7> vu56("vu", 150);
    const IntN<false, 8> vu64("vu", 160);
    const IntN<true, 1> vi8("vi", 91);
    const IntN<true, 2> vi16("vi", 101);
    const IntN<true, 3> vi24("vi", 111);
    const IntN<true, 4> vi32("vi", 121);
    const IntN<true, 5> vi40("vi", 131);
    const IntN<true, 6> vi48("vi", 141);
    const IntN<true, 7> vi56("vi", 151);
    const IntN<true, 8> vi64("vi", 161);
    const BigEndianN<false, 1> VU8("VU", 170);
    const BigEndianN<false, 2> VU16("VU", 180);
    const BigEndianN<false, 3> VU24("VU", 190);
    const BigEndianN<false, 4> VU32("VU", 200);
    const BigEndianN<false, 5> VU40("VU", 210);
    const BigEndianN<false, 6> VU48("VU", 220);
    const BigEndianN<false, 7> VU56("VU", 230);
    const BigEndianN<false, 8> VU64("VU", 240);
    const BigEndianN<true, 1> VI8("VI", 171);
    const BigEndianN<true, 2> VI16("VI", 181);
    const BigEndianN<true, 3> VI24("VI", 191);
    const BigEndianN<true, 4> VI32("VI", 201);
    const BigEndianN<true, 5> VI40("VI", 211);
    const BigEndianN<true, 6> VI48("VI", 221);
    const BigEndianN<true, 7> VI56("VI", 231);
    const BigEndianN<true, 8> VI64("VI", 241);

    EXPECT_EQ(vu8.static_offset(), 90u);
    EXPECT_EQ(vu16.static_offset(), 100u);
    EXPECT_EQ(vu24.static_offset(), 110u);
    EXPECT_EQ(vu32.static_offset(), 120u);
    EXPECT_EQ(vu40.static_offset(), 130u);
    EXPECT_EQ(vu48.static_offset(), 140u);
    EXPECT_EQ(vu56.static_offset(), 150u);
    EXPECT_EQ(vu64.static_offset(), 160u);
    EXPECT_EQ(vi8.static_offset(), 91u);
    EXPECT_EQ(vi16.static_offset(), 101u);
    EXPECT_EQ(vi24.static_offset(), 111u);
    EXPECT_EQ(vi32.static_offset(), 121u);
    EXPECT_EQ(vi40.static_offset(), 131u);
    EXPECT_EQ(vi48.static_offset(), 141u);
    EXPECT_EQ(vi56.static_offset(), 151u);
    EXPECT_EQ(vi64.static_offset(), 161u);
    EXPECT_EQ(VU8.static_offset(), 170u);
    EXPECT_EQ(VU16.static_offset(), 180u);
    EXPECT_EQ(VU24.static_offset(), 190u);
    EXPECT_EQ(VU32.static_offset(), 200u);
    EXPECT_EQ(VU40.static_offset(), 210u);
    EXPECT_EQ(VU48.static_offset(), 220u);
    EXPECT_EQ(VU56.static_offset(), 230u);
    EXPECT_EQ(VU64.static_offset(), 240u);
    EXPECT_EQ(VI8.static_offset(), 171u);
    EXPECT_EQ(VI16.static_offset(), 181u);
    EXPECT_EQ(VI24.static_offset(), 191u);
    EXPECT_EQ(VI32.static_offset(), 201u);
    EXPECT_EQ(VI40.static_offset(), 211u);
    EXPECT_EQ(VI48.static_offset(), 221u);
    EXPECT_EQ(VI56.static_offset(), 231u);
    EXPECT_EQ(VI64.static_offset(), 241u);

    EXPECT_EQ(vu8.dynamic_offset(TestUtil::ONE_BYTE), 90u);
    EXPECT_EQ(vu16.dynamic_offset(TestUtil::ONE_BYTE), 100u);
    EXPECT_EQ(vu24.dynamic_offset(TestUtil::ONE_BYTE), 110u);
    EXPECT_EQ(vu32.dynamic_offset(TestUtil::ONE_BYTE), 120u);
    EXPECT_EQ(vu40.dynamic_offset(TestUtil::ONE_BYTE), 130u);
    EXPECT_EQ(vu48.dynamic_offset(TestUtil::ONE_BYTE), 140u);
    EXPECT_EQ(vu56.dynamic_offset(TestUtil::ONE_BYTE), 150u);
    EXPECT_EQ(vu64.dynamic_offset(TestUtil::ONE_BYTE), 160u);
    EXPECT_EQ(vi8.dynamic_offset(TestUtil::ONE_BYTE), 91u);
    EXPECT_EQ(vi16.dynamic_offset(TestUtil::ONE_BYTE), 101u);
    EXPECT_EQ(vi24.dynamic_offset(TestUtil::ONE_BYTE), 111u);
    EXPECT_EQ(vi32.dynamic_offset(TestUtil::ONE_BYTE), 121u);
    EXPECT_EQ(vi40.dynamic_offset(TestUtil::ONE_BYTE), 131u);
    EXPECT_EQ(vi48.dynamic_offset(TestUtil::ONE_BYTE), 141u);
    EXPECT_EQ(vi56.dynamic_offset(TestUtil::ONE_BYTE), 151u);
    EXPECT_EQ(vi64.dynamic_offset(TestUtil::ONE_BYTE), 161u);
    EXPECT_EQ(VU8.dynamic_offset(TestUtil::ONE_BYTE), 170u);
    EXPECT_EQ(VU16.dynamic_offset(TestUtil::ONE_BYTE), 180u);
    EXPECT_EQ(VU24.dynamic_offset(TestUtil::ONE_BYTE), 190u);
    EXPECT_EQ(VU32.dynamic_offset(TestUtil::ONE_BYTE), 200u);
    EXPECT_EQ(VU40.dynamic_offset(TestUtil::ONE_BYTE), 210u);
    EXPECT_EQ(VU48.dynamic_offset(TestUtil::ONE_BYTE), 220u);
    EXPECT_EQ(VU56.dynamic_offset(TestUtil::ONE_BYTE), 230u);
    EXPECT_EQ(VU64.dynamic_offset(TestUtil::ONE_BYTE), 240u);
    EXPECT_EQ(VI8.dynamic_offset(TestUtil::ONE_BYTE), 171u);
    EXPECT_EQ(VI16.dynamic_offset(TestUtil::ONE_BYTE), 181u);
    EXPECT_EQ(VI24.dynamic_offset(TestUtil::ONE_BYTE), 191u);
    EXPECT_EQ(VI32.dynamic_offset(TestUtil::ONE_BYTE), 201u);
    EXPECT_EQ(VI40.dynamic_offset(TestUtil::ONE_BYTE), 211u);
    EXPECT_EQ(VI48.dynamic_offset(TestUtil::ONE_BYTE), 221u);
    EXPECT_EQ(VI56.dynamic_offset(TestUtil::ONE_BYTE), 231u);
    EXPECT_EQ(VI64.dynamic_offset(TestUtil::ONE_BYTE), 241u);

    EXPECT_EQ(vu8.static_size(), 1u);
    EXPECT_EQ(vu16.static_size(), 2u);
    EXPECT_EQ(vu24.static_size(), 3u);
    EXPECT_EQ(vu32.static_size(), 4u);
    EXPECT_EQ(vu40.static_size(), 5u);
    EXPECT_EQ(vu48.static_size(), 6u);
    EXPECT_EQ(vu56.static_size(), 7u);
    EXPECT_EQ(vu64.static_size(), 8u);
    EXPECT_EQ(vi8.static_size(), 1u);
    EXPECT_EQ(vi16.static_size(), 2u);
    EXPECT_EQ(vi24.static_size(), 3u);
    EXPECT_EQ(vi32.static_size(), 4u);
    EXPECT_EQ(vi40.static_size(), 5u);
    EXPECT_EQ(vi48.static_size(), 6u);
    EXPECT_EQ(vi56.static_size(), 7u);
    EXPECT_EQ(vi64.static_size(), 8u);
    EXPECT_EQ(VI8.static_size(), 1u);
    EXPECT_EQ(VI16.static_size(), 2u);
    EXPECT_EQ(VI24.static_size(), 3u);
    EXPECT_EQ(VI32.static_size(), 4u);
    EXPECT_EQ(VI40.static_size(), 5u);
    EXPECT_EQ(VI48.static_size(), 6u);
    EXPECT_EQ(VI56.static_size(), 7u);
    EXPECT_EQ(VI64.static_size(), 8u);
    EXPECT_EQ(VU8.static_size(), 1u);
    EXPECT_EQ(VU16.static_size(), 2u);
    EXPECT_EQ(VU24.static_size(), 3u);
    EXPECT_EQ(VU32.static_size(), 4u);
    EXPECT_EQ(VU40.static_size(), 5u);
    EXPECT_EQ(VU48.static_size(), 6u);
    EXPECT_EQ(VU56.static_size(), 7u);
    EXPECT_EQ(VU64.static_size(), 8u);

    EXPECT_EQ(vu8.dynamic_size(TestUtil::ONE_BYTE), 1u);
    EXPECT_EQ(vu16.dynamic_size(TestUtil::ONE_BYTE), 2u);
    EXPECT_EQ(vu24.dynamic_size(TestUtil::ONE_BYTE), 3u);
    EXPECT_EQ(vu32.dynamic_size(TestUtil::ONE_BYTE), 4u);
    EXPECT_EQ(vu40.dynamic_size(TestUtil::ONE_BYTE), 5u);
    EXPECT_EQ(vu48.dynamic_size(TestUtil::ONE_BYTE), 6u);
    EXPECT_EQ(vu56.dynamic_size(TestUtil::ONE_BYTE), 7u);
    EXPECT_EQ(vu64.dynamic_size(TestUtil::ONE_BYTE), 8u);
    EXPECT_EQ(vi8.dynamic_size(TestUtil::ONE_BYTE), 1u);
    EXPECT_EQ(vi16.dynamic_size(TestUtil::ONE_BYTE), 2u);
    EXPECT_EQ(vi24.dynamic_size(TestUtil::ONE_BYTE), 3u);
    EXPECT_EQ(vi32.dynamic_size(TestUtil::ONE_BYTE), 4u);
    EXPECT_EQ(vi40.dynamic_size(TestUtil::ONE_BYTE), 5u);
    EXPECT_EQ(vi48.dynamic_size(TestUtil::ONE_BYTE), 6u);
    EXPECT_EQ(vi56.dynamic_size(TestUtil::ONE_BYTE), 7u);
    EXPECT_EQ(vi64.dynamic_size(TestUtil::ONE_BYTE), 8u);
    EXPECT_EQ(VU8.dynamic_size(TestUtil::ONE_BYTE), 1u);
    EXPECT_EQ(VU16.dynamic_size(TestUtil::ONE_BYTE), 2u);
    EXPECT_EQ(VU24.dynamic_size(TestUtil::ONE_BYTE), 3u);
    EXPECT_EQ(VU32.dynamic_size(TestUtil::ONE_BYTE), 4u);
    EXPECT_EQ(VU40.dynamic_size(TestUtil::ONE_BYTE), 5u);
    EXPECT_EQ(VU48.dynamic_size(TestUtil::ONE_BYTE), 6u);
    EXPECT_EQ(VU56.dynamic_size(TestUtil::ONE_BYTE), 7u);
    EXPECT_EQ(VU64.dynamic_size(TestUtil::ONE_BYTE), 8u);
    EXPECT_EQ(VI8.dynamic_size(TestUtil::ONE_BYTE), 1u);
    EXPECT_EQ(VI16.dynamic_size(TestUtil::ONE_BYTE), 2u);
    EXPECT_EQ(VI24.dynamic_size(TestUtil::ONE_BYTE), 3u);
    EXPECT_EQ(VI32.dynamic_size(TestUtil::ONE_BYTE), 4u);
    EXPECT_EQ(VI40.dynamic_size(TestUtil::ONE_BYTE), 5u);
    EXPECT_EQ(VI48.dynamic_size(TestUtil::ONE_BYTE), 6u);
    EXPECT_EQ(VI56.dynamic_size(TestUtil::ONE_BYTE), 7u);
    EXPECT_EQ(VI64.dynamic_size(TestUtil::ONE_BYTE), 8u);
}

TEST(TestIntN, format8) {
    const IntN<true, 1> vi("vi", 0);
    const IntN<false, 1> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 5, "-102"); // 0x9A
    TestUtil::verifyFormat(vu, buffer + 4, "154");  // 0x9A
    const BigEndianN<true, 1> VI("VI", 0);
    const BigEndianN<false, 1> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-118"); // 0x8A
    TestUtil::verifyFormat(VU, buffer, "138");  // 0x8A
}

TEST(TestIntN, format16) {
    const IntN<true, 2> vi("vi", 0);
    const IntN<false, 2> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 5, "-17254"); // 0xBC9A
    TestUtil::verifyFormat(vu, buffer + 4, "48282");  // 0xBC9A
    const BigEndianN<true, 2> VI("VI", 0);
    const BigEndianN<false, 2> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-30190"); // 0x8A12
    TestUtil::verifyFormat(VU, buffer, "35346");  // 0x8A12
}

TEST(TestIntN, format24) {
    const IntN<true, 3> vi("vi", 0);
    const IntN<false, 3> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 5, "-2179942"); // 0xDEBC9A
    TestUtil::verifyFormat(vu, buffer + 4, "14597274"); // 0xDEBC9A
    const BigEndianN<true, 3> VI("VI", 0);
    const BigEndianN<false, 3> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-7728588"); // 0x8A1234
    TestUtil::verifyFormat(VU, buffer, "9048628");  // 0x8A1234
}

TEST(TestIntN, format32) {
    const IntN<true, 4> vi("vi", 0);
    const IntN<false, 4> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 5, "-253838182"); // 0xF0DEBC9A
    TestUtil::verifyFormat(vu, buffer + 4, "4041129114"); // 0xF0DEBC9A
    const BigEndianN<true, 4> VI("VI", 0);
    const BigEndianN<false, 4> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-1978518442"); // 0x8A123456
    TestUtil::verifyFormat(VU, buffer, "2316448854");  // 0x8A123456
}

TEST(TestIntN, format40) {
    const IntN<true, 5> vi("vi", 0);
    const IntN<false, 5> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 5, "-365326058342"); // 0xAAF0DEBC9A
    TestUtil::verifyFormat(vu, buffer + 4, "734185569434");  // 0xAAF0DEBC9A
    const BigEndianN<true, 5> VI("VI", 0);
    const BigEndianN<false, 5> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-506500721032"); // 0x8A12345678
    TestUtil::verifyFormat(VU, buffer, "593010906744");  // 0x8A12345678
}

TEST(TestIntN, format48) {
    const IntN<true, 6> vi("vi", 0);
    const IntN<false, 6> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 4, "-93523470935432"); // 0xAAF0DEBC9A78
    TestUtil::verifyFormat(vu, buffer + 3, "187951505775224"); // 0xAAF0DEBC9A78
    const BigEndianN<true, 6> VI("VI", 0);
    const BigEndianN<false, 6> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-129664184584038"); // 0x8A123456789A
    TestUtil::verifyFormat(VU, buffer, "151810792126618");  // 0x8A123456789A
}

TEST(TestIntN, format56) {
    const IntN<true, 7> vi("vi", 0);
    const IntN<false, 7> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 3, "-23942008559470506"); // 0xAAF0DEBC9A7856
    TestUtil::verifyFormat(vu, buffer + 2, "48115585478457430");  // 0xAAF0DEBC9A7856
    const BigEndianN<true, 7> VI("VI", 0);
    const BigEndianN<false, 7> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-33194031253513540"); // 0x8A123456789ABC
    TestUtil::verifyFormat(VU, buffer, "38863562784414396");  // 0x8A123456789ABC
}

TEST(TestIntN, format64) {
    const IntN<true, 8> vi("vi", 0);
    const IntN<false, 8> vu("vu", 1);
    TestUtil::verifyFormat(vi, buffer + 2, "-6129154191224449484"); // 0xAAF0DEBC9A785634
    TestUtil::verifyFormat(vu, buffer + 1, "12317589882485102132"); // 0xAAF0DEBC9A785634
    const BigEndianN<true, 8> VI("VI", 0);
    const BigEndianN<false, 8> VU("VU", 0);
    TestUtil::verifyFormat(VI, buffer, "-8497672000899466018"); // 0x8A123456789ABCDE
    TestUtil::verifyFormat(VU, buffer, "9949072072810085598");  // 0x8A123456789ABCDE
}

}
