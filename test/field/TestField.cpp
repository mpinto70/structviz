
#include "TestFieldUtil.h"

#include "field/Character.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace field {
namespace {
class FieldMock : public Field {
public:
    FieldMock(const std::string& name)
          : Field(name) {
    }
    MOCK_CONST_METHOD0(static_offset, size_t());
    MOCK_CONST_METHOD1(dynamic_offset, size_t(const uint8_t* buffer));
    MOCK_CONST_METHOD0(static_size, size_t());
    MOCK_CONST_METHOD1(dynamic_size, size_t(const uint8_t* buffer));
    MOCK_CONST_METHOD1(do_format, std::string(const uint8_t* buffer));
    MOCK_CONST_METHOD2(can_compute_offset_size, bool(const uint8_t* buffer, size_t buffer_size));
};
}

TEST(TestField, create) {
    const FieldMock mock("mock name");
    const Field& field = mock;
    EXPECT_EQ(field.name(), "mock name");
}

using ::testing::Return;

TEST(TestField, format) {
    const uint8_t buffer[] = { 's', 'o', 'm', 'e', ' ', 's', 't', 'r', 'i', 'n', 'g' };
    const FieldMock mock("mock name");
    const Field& field = mock;
    EXPECT_CALL(mock, do_format(buffer)).WillOnce(Return("formatted string"));

    EXPECT_EQ(field.format(buffer), "formatted string");
}
}
