
#pragma once
namespace ui {
class TestUtil {
public:
    static constexpr char INT_CONTENT_FOR_BUFFER[] =
          "  0 - 4d 41 52 43 45 4c 4f 20 ... | name    = MARCELO PINTO\n"
          " 13 - 00 01 02 03 04 20 06 07     | eight   = ..... ..\n"
          " 21 - 48                          | char    = H\n"
          " 22 - 48                          | value8  = 72\n"
          " 23 - 02 01                       | value16 = 258\n"
          " 25 - 11 22 33 44                 | value32 = 1144201745\n"
          " 29 - 01 23 45 67 89 ab cd ef     | value64 = 17279655951921914625\n";
    static constexpr char BE_CONTENT_FOR_BUFFER[] =
          "  0 - 4d 41 52 43 45 4c 4f 20 ... | name    = MARCELO PINTO\n"
          " 13 - 00 01 02 03 04 20 06 07     | eight   = ..... ..\n"
          " 21 - 48                          | char    = H\n"
          " 22 - 48                          | value8  = 72\n"
          " 23 - 02 01                       | value16 = 513\n"
          " 25 - 11 22 33 44                 | value32 = 287454020\n"
          " 29 - 01 23 45 67 89 ab cd ef     | value64 = 81985529216486895\n"
          " 37 - 47 41 52 42 41 47 45        | garbage = GARBAGE\n";
};
}
