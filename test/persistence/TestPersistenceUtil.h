#pragma once

#include "structure/BatchFormatter.h"

#include <string>

namespace persistence {
class TestUtil {
public:
    static constexpr char ALL_TYPES_SPEC[] =
          "char      | 13                      | value for fixed string\n"
          "char      |  1                      | value for single char\n"
          "int       |  8                      | value for int 8\n"
          "int       | 16                      | value for int 16\n"
          "int       | 24                      | value for int 24\n"
          "int       | 32                      | value for int 32\n"
          "int       | 40                      | value for int 40\n"
          "int       | 48                      | value for int 48\n"
          "int       | 56                      | value for int 56\n"
          "int       | 64                      | value for int 64\n"
          "uint      |  8                      | value for unsigned int 8\n"
          "uint      | 16                      | value for unsigned int 16\n"
          "uint      | 24                      | value for unsigned int 24\n"
          "uint      | 32                      | value for unsigned int 32\n"
          "uint      | 40                      | value for unsigned int 40\n"
          "uint      | 48                      | value for unsigned int 48\n"
          "uint      | 56                      | value for unsigned int 56\n"
          "uint      | 64                      | value for unsigned int 64\n"
          "be        |  8                      | value for big endian 8\n"
          "be        | 16                      | value for big endian 16\n"
          "be        | 24                      | value for big endian 24\n"
          "be        | 32                      | value for big endian 32\n"
          "be        | 40                      | value for big endian 40\n"
          "be        | 48                      | value for big endian 48\n"
          "be        | 56                      | value for big endian 56\n"
          "be        | 64                      | value for big endian 64\n"
          "ube       |  8                      | value for unsigned big endian 8\n"
          "ube       | 16                      | value for unsigned big endian 16\n"
          "ube       | 24                      | value for unsigned big endian 24\n"
          "ube       | 32                      | value for unsigned big endian 32\n"
          "ube       | 40                      | value for unsigned big endian 40\n"
          "ube       | 48                      | value for unsigned big endian 48\n"
          "ube       | 56                      | value for unsigned big endian 56\n"
          "ube       | 64                      | value for unsigned big endian 64\n"
          "IP        |  4                      | value IPV4\n"
          "hex       | 14                      | fixed hexadecimal\n"
          "utc       | 32                      | utc time in seconds\n"
          "utc       | 64                      | utc time in nano seconds\n"
          "uint      | 16                      | size_field\n"
          "uint      | 32                      | offset_field\n"
          "varstring | offset_field,size_field | a variable sized string\n";

    static constexpr size_t ALL_TYPES_STATIC_SIZE = 13 + 1 + (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8) * 4 + 4 + 14 + 4 + 8 + 2 + 4;

    static constexpr char INT_SPEC[] =
          "char | 13 | name   \n"
          "char |  8 | eight  \n"
          "char |  1 | char   \n"
          "int  |  8 | value8 \n"
          "int  | 16 | value16\n"
          "int  | 32 | value32\n"
          "int  | 64 | value64\n";

    static constexpr char BE_SPEC[] =
          "char | 13 | name   \n"
          "char |  8 | eight  \n"
          "char |  1 | char   \n"
          "be   |  8 | value8 \n"
          "be   | 16 | value16\n"
          "be   | 32 | value32\n"
          "be   | 64 | value64\n"
          "char |  7 | garbage\n";

    static constexpr char VARSTRING_SPEC[] =
          "char        | 13          | name   \n"
          "uint        |  8          | offset \n"
          "uint        |  8          | size   \n"
          "varstring   | offset,size | string \n";

    static constexpr char UTC_SPEC[] =
          "char | 13 | name\n"
          "utc  | 32 | dt1 \n"
          "utc  | 64 | dt2 \n";

    static std::string create_content_file_batch();
    static std::string create_spec_file_INT();
    static std::string create_spec_file_BE();
    static std::string create_spec_file_batch();
    static structure::BatchFormatter create_batch_from(const std::string& spec);
    static structure::BatchFormatter create_batch_from_spec();
};
}
