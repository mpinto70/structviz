#include "Driver.h"
#include "persistence/Loader.h"

namespace driver {

Driver::Driver(const std::string& batch_name, std::unique_ptr<ui::UI>&& ui)
      : m_batch_name(batch_name),
        m_batch(persistence::Loader::load_batch(batch_name)),
        m_ui(std::move(ui)),
        m_index(0),
        m_changed(false) {
}

void Driver::run(std::istream& data) {
    m_index = 0;
    ui::Operation oper;
    m_ui->show(m_batch, m_index, data);
    do {
        oper = m_ui->get_input();
        if (process(oper, data)) {
            m_ui->show(m_batch, m_index, data);
        }
    } while (oper != ui::Operation::Quit);
}

bool Driver::process(ui::Operation operation, std::istream&) {
    try {
        switch (operation) {
            case ui::Operation::NoOp:
                return false;
            case ui::Operation::Quit:
                if (m_changed) {
                    if (m_ui->want_to_save_first()) {
                        save_batch();
                    }
                }
                return false;
            case ui::Operation::Next:
                if (m_index + 1 < m_batch.nodes().size()) {
                    ++m_index;
                    return true;
                } else {
                    return false;
                }
            case ui::Operation::Previous:
                if (m_index > 0) {
                    --m_index;
                    return true;
                } else {
                    return false;
                }
            case ui::Operation::NextSame:
                if (not m_batch.nodes().empty()) {
                    position_same_origin(true);
                    return true;
                } else {
                    return false;
                }
            case ui::Operation::PreviousSame:
                if (not m_batch.nodes().empty()) {
                    position_same_origin(false);
                    return true;
                } else {
                    return false;
                }
            case ui::Operation::First:
                if (m_index > 0) {
                    m_index = 0;
                    return true;
                } else {
                    return false;
                }
            case ui::Operation::Last:
                if (m_index + 1 < m_batch.nodes().size()) {
                    m_index = m_batch.nodes().size() - 1;
                    return true;
                } else {
                    return false;
                }
            case ui::Operation::Delete: // delete current spec
                if (m_index < m_batch.nodes().size()) {
                    m_batch.remove(m_index);
                    if (m_index > 0) {
                        --m_index;
                    }
                    m_changed = true;
                }
                return true;
            case ui::Operation::New: { // insert a spec after current
                auto node = create_node(m_index);
                insert_node(std::move(node));
                m_changed = true;
                return true;
            }
            case ui::Operation::AddBuffer: { // add a buffer
                auto node = create_buffer(m_index);
                insert_node(std::move(node));
                m_changed = true;
                return true;
            }
            case ui::Operation::Save:
                save_batch();
                m_changed = false;
                return true;
            case ui::Operation::Reload:
                m_batch = persistence::Loader::load_batch(m_batch_name);
                if (m_index >= m_batch.nodes().size()) {
                    if (m_batch.nodes().empty()) {
                        m_index = 0;
                    } else {
                        m_index = m_batch.nodes().size() - 1;
                    }
                }
                m_changed = false;
                return true;
            case ui::Operation::Fix: { // fix current node
                auto node = fix_node(m_index);
                replace_node(std::move(node));
                m_changed = true;
                return true;
            }
            case ui::Operation::Duplicate: { // append current node at the end
                duplicate_node_at_end();
                m_changed = true;
                return true;
            }
        }
        return true;
    } catch (std::exception& e) {
        m_ui->show_exception_and_wait_confirm("An error occurred", e);
        return false;
    }
}

void Driver::show_exception(const std::string& msg, const std::exception& e) const {
    m_ui->show_exception_and_wait_confirm(msg, e);
}

void Driver::insert_node(structure::Struct&& node) {
    auto& nodes = m_batch.nodes();
    if (nodes.empty()) {
        nodes.push_back(std::move(node));
    } else {
        nodes.insert(nodes.begin() + m_index + 1, std::move(node));
    }
    if (m_index + 1 < nodes.size()) {
        ++m_index;
    }
}

structure::Struct Driver::create_node(const size_t index) const {
    const auto params = m_ui->get_node_parameters(m_batch, index);
    return structure::Struct(params.offset, params.name, persistence::Loader::load_struct(params.path));
}

void Driver::replace_node(structure::Struct&& node) {
    auto& nodes = m_batch.nodes();
    nodes.erase(nodes.begin() + m_index);
    nodes.insert(nodes.begin() + m_index, std::move(node));
}

structure::Struct Driver::fix_node(const size_t index) const {
    const auto params = m_ui->get_fixed_parameters(m_batch, index);
    return structure::Struct(params.offset, params.name, persistence::Loader::load_struct(params.path));
}

structure::Struct Driver::create_buffer(const size_t index) const {
    const int size = m_ui->get_buffer_size();
    auto& nodes = m_batch.nodes();
    if (nodes.empty()) {
        return persistence::Loader::build_node(0, size);
    } else {
        size_t offset = nodes[index].offset() + nodes[index].formatter().static_size();
        return persistence::Loader::build_node(offset, size);
    }
}

void Driver::duplicate_node_at_end() {
    const auto& current_node = m_batch.nodes().at(m_index);
    const auto& last_node = m_batch.nodes().back();

    structure::Struct newnode(last_node.offset() + last_node.formatter().static_size(),
          current_node.name(),
          persistence::Loader::load_struct(current_node.origin()));
    m_batch.nodes().push_back(std::move(newnode));
    m_index = m_batch.nodes().size() - 1;
}

void Driver::save_batch() const {
    const auto path = m_ui->get_batch_name(m_batch_name);
    persistence::Loader::save(m_batch, path);
    m_ui->wait_confirm("File saved.");
}

void Driver::position_same_origin(bool forward) {
    const auto& nodes = batch().nodes();
    const int step = forward ? 1 : -1;
    const std::string& origin = nodes.at(m_index).origin();
    const int size = int(nodes.size());
    for (int idx = int(m_index) + step; idx >= 0 && idx < size; idx += step) {
        if (nodes.at(size_t(idx)).origin() == origin) {
            m_index = size_t(idx);
            return;
        }
    }
}
}
