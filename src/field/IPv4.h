#pragma once

#include "field/StaticDimensions.h"

namespace field {

class IPv4 : public StaticDimensions {
public:
    IPv4(std::string name, size_t offset);
    ~IPv4() noexcept override = default;

private:
    std::string do_format(const uint8_t* buffer) const override;
};
}
