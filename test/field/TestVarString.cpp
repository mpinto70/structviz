
#include "TestFieldUtil.h"

#include "field/VarString.h"

#include <gtest/gtest.h>

namespace field {

using ::testing::Return;

TEST(TestVarString, print) {
    const uint8_t buffer[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    const UnsignedMock num1("offset_field", 1, 2);
    const UnsignedMock num2("size_field", 3, 4);
    const VarString vs1("vs1", &num1, &num2);
    const VarString vs2("vs2", &num2, &num1);

    EXPECT_CALL(num1, value(buffer)).WillOnce(Return(3));
    EXPECT_CALL(num2, value(buffer)).WillOnce(Return(4));

    TestUtil::verifyFormat(vs1, buffer, "3456");

    EXPECT_CALL(num1, value(buffer)).WillOnce(Return(2));
    EXPECT_CALL(num2, value(buffer)).WillOnce(Return(5));

    TestUtil::verifyFormat(vs2, buffer, "56");
}
}
