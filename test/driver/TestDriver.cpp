
#include "TestDriver.h"

#include "../test/field/TestFieldUtil.h"
#include "../test/persistence/TestPersistenceUtil.h"
#include "../test/util/TestUtil.h"

#include "driver/Driver.h"
#include "persistence/Loader.h"
#include "ui/UI.h"

#include "gmock/gmock.h"

#include <fstream>
#include <unistd.h>

namespace driver {

class MockUI : public ui::UI {
public:
    MOCK_CONST_METHOD0(get_input, ui::Operation());
    MOCK_METHOD3(show, void(const structure::BatchFormatter& batch, size_t index, std::istream& data));
    MOCK_CONST_METHOD2(get_node_parameters, ui::UI::NodeParameters(const structure::BatchFormatter& batch, size_t index));
    MOCK_CONST_METHOD2(get_fixed_parameters, ui::UI::NodeParameters(const structure::BatchFormatter& batch, size_t index));
    MOCK_CONST_METHOD0(get_buffer_size, int());
    MOCK_CONST_METHOD1(get_batch_name, std::string(const std::string& current_name));
    MOCK_CONST_METHOD2(show_exception_and_wait_confirm, void(const std::string& msg, const std::exception& e));
    MOCK_CONST_METHOD1(wait_confirm, void(const std::string& msg));
    MOCK_CONST_METHOD0(want_to_save_first, bool());
};

void TestDriver::SetUp() {
    pmock.reset(new MockUI);
    const auto name_bin = util::TestUtil::create_binary_file(field::TestUtil::BUFFER);
    data.reset(new std::ifstream(name_bin, std::ios::binary));
    batch_name = persistence::TestUtil::create_spec_file_batch();
}

void TestDriver::TearDown() {
}

void verify_batches(const std::string& msg, const structure::BatchFormatter& lhs, const structure::BatchFormatter& rhs) {
    const auto& lnodes = lhs.nodes();
    const auto& rnodes = rhs.nodes();
    EXPECT_EQ(lnodes.size(), rnodes.size()) << msg;
    for (size_t n = 0; n < lnodes.size() && n < rnodes.size(); ++n) {
        const std::string msgn = msg + " for node [" + std::to_string(n) + "]";
        const auto& lnode = lnodes[n];
        const auto& rnode = rnodes[n];
        EXPECT_EQ(lnode.name(), rnode.name()) << msgn;
        EXPECT_EQ(lnode.offset(), rnode.offset()) << msgn;

        const auto& lformatter = lnode.formatter();
        const auto& rformatter = rnode.formatter();
        EXPECT_EQ(lformatter.origin(), rformatter.origin()) << msgn;
        const auto& lfields = lformatter.fields();
        const auto& rfields = rformatter.fields();
        EXPECT_EQ(lfields.size(), rfields.size()) << msgn;
        for (size_t f = 0; f < lfields.size() && f < rfields.size(); ++f) {
            const std::string msgf = msgn + " for field [" + std::to_string(f) + "]";
            const auto& lfield = *lfields[f];
            const auto& rfield = *rfields[f];
            // same name, same size and same type
            EXPECT_EQ(lfield.name(), rfield.name()) << msgf;
            EXPECT_EQ(lfield.static_size(), rfield.static_size()) << msgf;
            EXPECT_EQ(typeid(lfield).hash_code(), typeid(rfield).hash_code()) << msgf;
        }
    }
}

using ::testing::_;
using ::testing::InSequence;
using ::testing::Return;
TEST_F(TestDriver, run_and_quit) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillRepeatedly(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_noop) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::NoOp));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_next) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_next_same) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::NextSame));
    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::NextSame));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::NextSame));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_previous) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Previous));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_previous_same) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::PreviousSame));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::PreviousSame));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_next_previous) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Previous));
    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_next_upto_end) {
    InSequence dummy;
    auto batch = persistence::TestUtil::create_batch_from(batch_name);

    for (size_t i = 0; i < batch.nodes().size(); ++i) {
        EXPECT_CALL(*pmock, show(_, i, _));
        EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    }
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_last) {
    InSequence dummy;

    auto batch = persistence::TestUtil::create_batch_from(batch_name);

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Last));
    EXPECT_CALL(*pmock, show(_, batch.nodes().size() - 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_last_and_first) {
    InSequence dummy;

    auto batch = persistence::TestUtil::create_batch_from(batch_name);

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Last));
    EXPECT_CALL(*pmock, show(_, batch.nodes().size() - 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::First));
    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    verify_batches("", drv.batch(), persistence::TestUtil::create_batch_from(batch_name));
}

TEST_F(TestDriver, run_and_insert_node) {
    InSequence dummy;
    const auto struct_path = util::TestUtil::create_text_file(persistence::TestUtil::INT_SPEC);

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::New));
    EXPECT_CALL(*pmock, get_node_parameters(_, 1)).WillOnce(Return(ui::UI::NodeParameters{ 10, "some name", struct_path }));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));
    EXPECT_CALL(*pmock, want_to_save_first()).WillOnce(Return(false));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    auto final_batch = persistence::TestUtil::create_batch_from(batch_name);
    auto& nodes = final_batch.nodes();
    nodes.insert(nodes.begin() + 2, structure::Struct(10, "some name", persistence::Loader::load_struct(struct_path)));
    verify_batches("", drv.batch(), final_batch);
}

TEST_F(TestDriver, run_and_insert_buffer) {
    InSequence dummy;
    const size_t buffer_size = 50;
    const auto struct_path = util::TestUtil::create_text_file(persistence::TestUtil::INT_SPEC);

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::AddBuffer));
    EXPECT_CALL(*pmock, get_buffer_size()).WillOnce(Return(buffer_size));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));
    EXPECT_CALL(*pmock, want_to_save_first()).WillOnce(Return(false));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    auto final_batch = persistence::TestUtil::create_batch_from(batch_name);
    auto& nodes = final_batch.nodes();
    const size_t buffer_offset = nodes[1].offset() + nodes[1].formatter().static_size();
    nodes.insert(nodes.begin() + 2, persistence::Loader::build_node(buffer_offset, buffer_size));
    verify_batches("", drv.batch(), final_batch);
}

TEST_F(TestDriver, run_and_delete) {
    InSequence dummy;
    const auto struct_path = util::TestUtil::create_text_file(persistence::TestUtil::INT_SPEC);

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Delete));
    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));
    EXPECT_CALL(*pmock, want_to_save_first()).WillOnce(Return(false));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    auto final_batch = persistence::TestUtil::create_batch_from(batch_name);
    auto& nodes = final_batch.nodes();
    nodes.erase(nodes.begin() + 1);
    verify_batches("", drv.batch(), final_batch);
}

TEST_F(TestDriver, run_and_save) {
    InSequence dummy;
    const size_t buffer_size = 50;
    const auto struct_path = util::TestUtil::create_text_file(persistence::TestUtil::INT_SPEC);
    const auto saved_path = util::TestUtil::create_text_file();

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::AddBuffer));
    EXPECT_CALL(*pmock, get_buffer_size()).WillOnce(Return(buffer_size));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Save));
    EXPECT_CALL(*pmock, get_batch_name(batch_name)).WillOnce(Return(saved_path));
    EXPECT_CALL(*pmock, wait_confirm("File saved."));
    EXPECT_CALL(*pmock, show(_, 2, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);

    auto final_batch = persistence::Loader::load_batch(saved_path);
    verify_batches("content loaded", drv.batch(), final_batch);
}

TEST_F(TestDriver, run_and_duplicate_first) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Duplicate));
    EXPECT_CALL(*pmock, show(_, 3, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));
    EXPECT_CALL(*pmock, want_to_save_first()).WillOnce(Return(false));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    auto final_batch = persistence::TestUtil::create_batch_from(batch_name);
    auto& nodes = final_batch.nodes();
    const size_t duplicated_offset = nodes[2].offset() + nodes[2].formatter().static_size(); // after the last
    nodes.push_back(structure::Struct(duplicated_offset, nodes[0].name(), persistence::Loader::load_struct(nodes[0].origin())));
    verify_batches("", drv.batch(), final_batch);
}

TEST_F(TestDriver, run_and_duplicate_second) {
    InSequence dummy;

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Duplicate));
    EXPECT_CALL(*pmock, show(_, 3, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));
    EXPECT_CALL(*pmock, want_to_save_first()).WillOnce(Return(false));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    auto final_batch = persistence::TestUtil::create_batch_from(batch_name);
    auto& nodes = final_batch.nodes();
    const size_t duplicated_offset = nodes[2].offset() + nodes[2].formatter().static_size(); // after the last
    nodes.push_back(structure::Struct(duplicated_offset, nodes[1].name(), persistence::Loader::load_struct(nodes[1].origin())));
    verify_batches("", drv.batch(), final_batch);
}

TEST_F(TestDriver, run_and_fix_node) {
    InSequence dummy;
    const auto struct_path = util::TestUtil::create_text_file(persistence::TestUtil::INT_SPEC);

    EXPECT_CALL(*pmock, show(_, 0, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Next));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Fix));
    EXPECT_CALL(*pmock, get_fixed_parameters(_, 1)).WillOnce(Return(ui::UI::NodeParameters{ 10, "other name", struct_path }));
    EXPECT_CALL(*pmock, show(_, 1, _));
    EXPECT_CALL(*pmock, get_input()).WillOnce(Return(ui::Operation::Quit));
    EXPECT_CALL(*pmock, want_to_save_first()).WillOnce(Return(false));

    Driver drv(batch_name, std::move(pmock));
    drv.run(*data);
    auto final_batch = persistence::TestUtil::create_batch_from(batch_name);
    auto& nodes = final_batch.nodes();
    nodes.erase(nodes.begin() + 1);
    nodes.insert(nodes.begin() + 1, structure::Struct(10, "other name", persistence::Loader::load_struct(struct_path)));
    verify_batches("", drv.batch(), final_batch);
}
}
