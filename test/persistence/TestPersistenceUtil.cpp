
#include "TestPersistenceUtil.h"
#include "../test/field/TestFieldUtil.h"
#include "../test/structure/TestStructureUtil.h"
#include "../test/util/TestUtil.h"

#include "persistence/Loader.h"

namespace persistence {

constexpr char TestUtil::INT_SPEC[];
constexpr char TestUtil::BE_SPEC[];
constexpr char TestUtil::VARSTRING_SPEC[];
constexpr char TestUtil::UTC_SPEC[];

std::string TestUtil::create_content_file_batch() {
    return util::TestUtil::create_binary_file(structure::TestUtil::create_content_for_batch());
}

std::string TestUtil::create_spec_file_INT() {
    return util::TestUtil::create_text_file(INT_SPEC);
}

std::string TestUtil::create_spec_file_BE() {
    return util::TestUtil::create_text_file(BE_SPEC);
}

std::string TestUtil::create_spec_file_batch() {
    const auto int_name = create_spec_file_INT();
    const auto be_name = create_spec_file_BE();
    const auto content = " 1|int 1|" + int_name + "\n"
                         + std::to_string(2 + 1 * sizeof(field::TestUtil::BUFFER)) + "| be 1|" + be_name + "\n"
                         + std::to_string(2 + 2 * sizeof(field::TestUtil::BUFFER)) + "| be 2|" + be_name + "\n";
    return util::TestUtil::create_text_file(content);
}

structure::BatchFormatter TestUtil::create_batch_from(const std::string& spec) {
    return Loader::load_batch(spec);
}

structure::BatchFormatter TestUtil::create_batch_from_spec() {
    return create_batch_from(create_spec_file_batch());
}
}
