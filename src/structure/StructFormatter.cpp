#include "StructFormatter.h"

#include <algorithm>
#include <numeric>

namespace structure {

namespace {
size_t sum_size(size_t sum, const std::unique_ptr<field::Field>& rhs) {
    return sum + rhs->static_size();
}

bool compare_name(const FormattedField& lhs, const FormattedField& rhs) {
    return lhs.name.size() < rhs.name.size();
}

struct SizeCompare {
    explicit SizeCompare(const uint8_t* buffer)
          : buffer(buffer) {}
    bool operator()(const std::unique_ptr<field::Field>& lhs, const std::unique_ptr<field::Field>& rhs) const {
        return (lhs->dynamic_offset(buffer) + lhs->dynamic_size(buffer))
               < (rhs->dynamic_offset(buffer) + rhs->dynamic_size(buffer));
    }

private:
    const uint8_t* buffer;
};
}

StructFormatter::StructFormatter(std::string origin, FieldListT&& fields)
      : m_origin(std::move(origin)),
        m_fields(std::move(fields)),
        m_size(0) {
    m_size = std::accumulate(m_fields.begin(), m_fields.end(), 0u, sum_size);
}

StructFormatter::FormattedFieldListT StructFormatter::format(const uint8_t* buffer, size_t buffer_size) const {
    FormattedFieldListT formatted;
    for (const auto& field : m_fields) {
        if (field->is_valid(buffer, buffer_size)) {
            const size_t offset = field->dynamic_offset(buffer);
            const size_t size = field->dynamic_size(buffer);

            formatted.emplace_back(field->name(), offset, size, field->format(buffer));
        } else {
            formatted.emplace_back(field->name());
        }
    }
    return formatted;
}

size_t StructFormatter::dynamic_size(const uint8_t* buffer) const {
    const auto it = std::max_element(m_fields.begin(), m_fields.end(), SizeCompare(buffer));
    return (*it)->dynamic_offset(buffer) + (*it)->dynamic_size(buffer);
}

size_t StructFormatter::maximum_name_size(const FormattedFieldListT& items) {
    if (items.empty()) {
        return 0;
    }
    return std::max_element(items.begin(), items.end(), compare_name)->name.size();
}
}
