#include "IPv4.h"

namespace field {
IPv4::IPv4(std::string name, const size_t offset)
      : StaticDimensions(std::move(name), offset, 4) {
}

std::string IPv4::do_format(const uint8_t* buffer) const {
    const auto off = dynamic_offset(buffer);
    return std::to_string(unsigned(buffer[off + 0]))
           + "." + std::to_string(unsigned(buffer[off + 1]))
           + "." + std::to_string(unsigned(buffer[off + 2]))
           + "." + std::to_string(unsigned(buffer[off + 3]));
}
}
