#pragma once

#include "field/Character.h"
#include "field/DynamicDimensions.h"

namespace field {
class VarString : public DynamicDimensions {
public:
    VarString(std::string name,
          const Numeric<false>* offset_field,
          const Numeric<false>* size_field);
    ~VarString() noexcept override = default;

private:
    const Character m_character;

    std::string do_format(const uint8_t* buffer) const override;
};
}
