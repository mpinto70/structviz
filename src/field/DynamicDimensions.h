#pragma once

#include "field/Numeric.h"

namespace field {

class DynamicDimensions : public Field {
public:
    DynamicDimensions(std::string name,
          const Numeric<false>* offset_field,
          const Numeric<false>* size_field)
          : Field(std::move(name)),
            m_offset_field(offset_field),
            m_size_field(size_field) {
    }
    ~DynamicDimensions() noexcept override = default;
    size_t static_offset() const override { return 0; }
    size_t dynamic_offset(const uint8_t* buffer) const override {
        return m_offset_field->value(buffer);
    }
    size_t static_size() const override { return 0; }
    size_t dynamic_size(const uint8_t* buffer) const override {
        return m_size_field->value(buffer);
    }

private:
    const Numeric<false>* m_offset_field;
    const Numeric<false>* m_size_field;
    bool can_compute_offset_size(const uint8_t* buffer, size_t buffer_size) const override {
        return m_offset_field->is_valid(buffer, buffer_size)
               && m_size_field->is_valid(buffer, buffer_size);
    }
};
}
